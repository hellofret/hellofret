<?php

namespace Hellofret\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HellofretUserBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
	
}
