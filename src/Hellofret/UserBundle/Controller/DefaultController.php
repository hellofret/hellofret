<?php

namespace Hellofret\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('HellofretUserBundle:Default:index.html.twig');
    }
}
