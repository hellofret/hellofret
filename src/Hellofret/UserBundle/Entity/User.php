<?php

namespace Hellofret\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Hellofret\UserBundle\Entity\Profile;

/**
 * User
 *
 * @ORM\Table(name="hel_user")
 * @ORM\Entity(repositoryClass="Hellofret\UserBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**

	* @ORM\OneToOne(targetEntity="Hellofret\UserBundle\Entity\Profile", cascade={"persist"})
	* 
	*/
	private $profile;
	

	
	public function __construct()
   {
       parent::__construct();
       // your own logic
   }
   
   public function isGranted($role)
    {
        return in_array($role, $this->getRoles());
    }
	
    /**
     * Get id
     *
     * @return int
     */
	 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set profile
     *
     * @param Profile $typeUser
     *
     * @return User
     */
    public function setProfile(Profile $profile = null)
	  {
		$this->profile = $profile;
	  }

    /**
     * Get profile
     *
     * @return Profile
     */
    public function getProfile()

	  {
		return $this->profile;
	  }
}
