<?php

namespace Hellofret\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hellofret\BackEndBundle\Entity\Image ;

/**
 * Profile
 *
 * @ORM\Table(name="hel_profile")
 * @ORM\Entity(repositoryClass="Hellofret\UserBundle\Repository\ProfileRepository")
 */
class Profile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
     * @ORM\Column(name="type_user", type="string", length=255, nullable=true)
     */
    private $typeUser;
	
	
	/**
     * @ORM\Column(name="nom_commercial", type="string", length=255, nullable=true)
     */
    private $nomCommercial;
	
	/**
     * @ORM\Column(name="raison_sociale", type="string", length=255, nullable=true)
     */
    private $raisonSociale;

    /**
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(name="telephone2", type="string", length=255, nullable=true)
     */
    private $telephone2;

    /**
     * @ORM\Column(name="site_web", type="string", length=255, nullable=true)
     */
    private $siteWeb;

    /**
     * @ORM\Column(name="metier", type="string", length=255, nullable=true)
     */
    private $metier;

    /**
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\Image", cascade={"persist"})
     */
    private $logo;

    /**
     * @ORM\Column(name="paiement_prestation", type="boolean", nullable=true)
     */
    private $paiementPrestation;

    /**
     * @ORM\Column(name="nom_responsable", type="string", length=255, nullable=true)
     */
    private $nomResponsable;

    /**
     * @ORM\Column(name="date_inscription", type="datetime", nullable=true)
     */
    private $dateInscription;

   
   public function __construct()
	{
		$this->dateInscription = new \Datetime();
	}
   
	
	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomCommercial
     *
     * @param string $nomCommercial
     *
     * @return Profile
     */
    public function setNomCommercial($nomCommercial)
    {
        $this->nomCommercial = $nomCommercial;

        return $this;
    }

    /**
     * Get nomCommercial
     *
     * @return string
     */
    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    /**
     * Set raisonSociale
     *
     * @param string $raisonSociale
     *
     * @return Profile
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    /**
     * Get raisonSociale
     *
     * @return string
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Profile
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Profile
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Profile
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set telephone2
     *
     * @param string $telephone2
     *
     * @return Profile
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return Profile
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * Set metier
     *
     * @param string $metier
     *
     * @return Profile
     */
    public function setMetier($metier)
    {
        $this->metier = $metier;

        return $this;
    }

    /**
     * Get metier
     *
     * @return string
     */
    public function getMetier()
    {
        return $this->metier;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Profile
     */
    public function setLogo(Image $logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set paiementPrestation
     *
     * @param boolean $paiementPrestation
     *
     * @return Profile
     */
    public function setPaiementPrestation($paiementPrestation)
    {
        $this->paiementPrestation = $paiementPrestation;

        return $this;
    }

    /**
     * Get paiementPrestation
     *
     * @return boolean
     */
    public function getPaiementPrestation()
    {
        return $this->paiementPrestation;
    }

    /**
     * Set nomResponsable
     *
     * @param string $nomResponsable
     *
     * @return Profile
     */
    public function setNomResponsable($nomResponsable)
    {
        $this->nomResponsable = $nomResponsable;

        return $this;
    }

    /**
     * Get nomResponsable
     *
     * @return string
     */
    public function getNomResponsable()
    {
        return $this->nomResponsable;
    }

    /**
     * Set dateInscription
     *
     * @param \DateTime $dateInscription
     *
     * @return Profile
     */
    public function setDateInscription($dateInscription)
    {
        $this->dateInscription = $dateInscription;

        return $this;
    }

    /**
     * Get dateInscription
     *
     * @return \DateTime
     */
    public function getDateInscription()
    {
        return $this->dateInscription;
    }

    /**
     * Set typeUser
     *
     * @param string $typeUser
     *
     * @return Profile
     */
    public function setTypeUser($typeUser)
    {
        $this->typeUser = $typeUser;

        return $this;
    }

    /**
     * Get typeUser
     *
     * @return string
     */
    public function getTypeUser()
    {
        return $this->typeUser;
    }
}
