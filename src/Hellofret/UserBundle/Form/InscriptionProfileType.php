<?php

namespace Hellofret\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscriptionProfileType extends ProfileType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
	  {
		$builder->remove('raisonSociale');
		$builder->remove('adresse');
		$builder->remove('ville');
		$builder->remove('siteWeb');
		$builder->remove('metier');
		$builder->remove('logo');
		$builder->remove('paiementPrestation');
	  }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\UserBundle\Entity\Profile'
        ));
    }
	
	public function getParent()
  	{
    	return new ProfileType();
  	}
}
