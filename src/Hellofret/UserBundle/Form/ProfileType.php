<?php

namespace Hellofret\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Hellofret\BackEndBundle\Form\ImageType;

class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeUser','hidden')
			->add('nomCommercial')
            ->add('raisonSociale')
            ->add('adresse')
            ->add('ville')
            ->add('telephone')
            ->add('telephone2')
            ->add('siteWeb')
            ->add('metier')
            ->add('logo', new ImageType(), array('required' => false))
            ->add('paiementPrestation')
            ->add('nomResponsable')
            //->add('dateInscription', 'datetime')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\UserBundle\Entity\Profile'
        ));
    }
}
