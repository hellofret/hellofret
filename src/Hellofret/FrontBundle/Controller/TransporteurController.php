<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TransporteurController extends Controller
{
    public function indexAction()
    {
        // Page Template
		return $this->render('HellofretFrontBundle:Transporteur:index.html.twig');
    }
	public function viewAction($id)
    {
        // Page Template
		return $this->render('HellofretFrontBundle:Transporteur:view.html.twig');
    }
	public function addAction(Request $request)
    {
        // Page Template
		return $this->render('HellofretFrontBundle:Transporteur:add.html.twig');
    }
	public function editAction($id, Request $request)
    {
        // Page Template
		return $this->render('HellofretFrontBundle:Transporteur:edit.html.twig');
    }
	public function deleteAction($id)
    {
        // Page Template
		return $this->render('HellofretFrontBundle:Transporteur:delete.html.twig');
    }
}
