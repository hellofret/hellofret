<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PagesController extends Controller
{
    public function ConditionsAction()
    {
    	return $this->render('HellofretFrontBundle:Pages:conditions.html.twig');
    }
	
	
	public function MentionsAction()
    {
    	return $this->render('HellofretFrontBundle:Pages:mentions.html.twig');
    }
	
	public function QuiSommesAction()
    {
    	return $this->render('HellofretFrontBundle:Pages:quisommesnous.html.twig');
    }
}
