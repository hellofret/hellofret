<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$trajets = $repository->trajetsDisponible();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$modes = $repository->findAll();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$mode1 = $repository->findBy(array('id' => 1));
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:CategorieMode');
		
		$categories = $repository->findBy(array('mode' => $mode1));
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$mode5 = $repository->findBy(array('id' => 5), array('id' => 'ASC'));
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:CategorieMode');
		
		$categoriesVehicules = $repository->findBy(array('mode' => $mode5));
		
		
		
		return $this->render('HellofretFrontBundle:Index:index.html.twig', array('trajets' => $trajets, "modes" => $modes, "categories" => $categories, "categoriesvehicules" => $categoriesVehicules));
    }
}
