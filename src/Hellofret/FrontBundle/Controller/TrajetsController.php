<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


use Hellofret\BackEndBundle\Entity\Trajet;


class TrajetsController extends Controller
{
    public function indexAction()
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$trajets = $repository->trajetsDisponible();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$modes = $repository->findAll();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$mode1 = $repository->findBy(array('id' => 1));
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:CategorieMode');
		
		$categories = $repository->findBy(array('mode' => $mode1));
		
		// Page Accueil
		return $this->render('HellofretFrontBundle:Trajet:index.html.twig', array('trajets' => $trajets, "modes" => $modes, "categories" => $categories) );
    }
	
	public function TrajetViewAction($id, Request $request)
    {
		
		if ( $this->get('security.context')->isGranted('ROLE_CHARGEUR') ){
			
			return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $id ) );
			
		}elseif( $this->get('security.context')->isGranted('ROLE_TRANSPORTEUR') ){
			return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $id ) );
		}
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		if( !$trajet ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Trajet:viewTrajet.html.twig', array("trajet" => $trajet));
    }
	
	public function SearchAction(Request $request)
    {
        
		if ($request->isMethod('POST'))
		{
		
		  	$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
			//get paramètre
			$mode = $request->request->get('mode') ;
			$categorie = $request->request->get('categorie') ;
			$depart = $request->request->get('depart') ;
			$arrivee = $request->request->get('arrivee') ;
			$dateDepart = $request->request->get('d-depart') ;
			$dateArrivee = $request->request->get('d-arrivee') ;
			
			$trajets = $repository->TrajetSearch($mode,$categorie,$depart,$arrivee,$dateDepart,$dateArrivee);
			
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
			$modes = $repository->findAll();
			
			$repository = $this
						->getDoctrine()
						->getManager()
						->getRepository('HellofretBackEndBundle:Mode');
			
			$mode1 = $repository->findBy(array('id' => 1));
			
			
			$repository = $this
						->getDoctrine()
						->getManager()
						->getRepository('HellofretBackEndBundle:CategorieMode');
			
			$categories = $repository->findBy(array('mode' => $mode1));
			
			
			
			return $this->render('HellofretFrontBundle:Trajet:search.html.twig', array('trajets' => $trajets, "modes" => $modes, "categories" => $categories) );
		
		}
		
		return $this->redirectToRoute('hellofret_front_trajets');
    }
	
	
}
