<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InscriptionController extends Controller
{
    public function inscriptionAction()
    {
        return $this->render('HellofretFrontBundle:Inscription:inscription.html.twig');
    }
}
