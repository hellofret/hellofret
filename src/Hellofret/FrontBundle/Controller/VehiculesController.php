<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


use Hellofret\BackEndBundle\Entity\VoitureLocation;

class VehiculesController extends Controller
{
    public function indexAction()
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');

		$voitures = $repository->findBy(array("disponible" => true ));
		
		
		
		// Page Accueil
		return $this->render('HellofretFrontBundle:Voiture:index.html.twig', array('voitures' => $voitures) );
    }
	
	public function VehiculeViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Location:viewVoiture.html.twig', array("voiture" => $voiture));
    }
	
	public function SearchAction(Request $request)
    {
        
		if ($request->isMethod('POST'))
		{
		
		  	$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
			//get paramètre
			$categorie = $request->request->get('categorie') ;
			$marque = $request->request->get('marque') ;
			$modele = $request->request->get('modele') ;

			
			$voitures = $repository->VoitureSearch($categorie,$marque,$modele);
			
			return $this->render('HellofretFrontBundle:Voiture:index.html.twig', array('voitures' => $voitures) );
		
		}
		
		return $this->redirectToRoute('hellofret_front_vehicules');
    }
	
	
}
