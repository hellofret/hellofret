<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FretController extends Controller
{
    public function indexAction()
    {
        // Page Accueil
		return $this->render('HellofretFrontBundle:Fret:index.html.twig');
    }
}
