<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


use Hellofret\BackEndBundle\Entity\FretAlimentation;
use Hellofret\BackEndBundle\Entity\FretAnimaux;
use Hellofret\BackEndBundle\Entity\FretBoisSable;
use Hellofret\BackEndBundle\Entity\FretCerealEpice;
use Hellofret\BackEndBundle\Entity\FretConteneur;
use Hellofret\BackEndBundle\Entity\FretDechet;
use Hellofret\BackEndBundle\Entity\FretHydrocarbure;
use Hellofret\BackEndBundle\Entity\FretConditionne;
use Hellofret\BackEndBundle\Entity\FretPalettes;
use Hellofret\BackEndBundle\Entity\FretVehicule;
use Hellofret\BackEndBundle\Entity\FretDemenagement;
use Hellofret\BackEndBundle\Entity\FretEquipement;
use Hellofret\BackEndBundle\Entity\FretMessagerie;
use Hellofret\BackEndBundle\Entity\FretPharmacie;

class FretsController extends Controller
{
    public function indexAction()
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$frets = $repository->FretDisponible();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$modes = $repository->findAll();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
		$mode1 = $repository->findBy(array('id' => 1));
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:CategorieMode');
		
		$categories = $repository->findBy(array('mode' => $mode1));
		
		// Page Accueil
		return $this->render('HellofretFrontBundle:Fret:index.html.twig', array('frets' => $frets, "modes" => $modes, "categories" => $categories) );
    }
	
	
	public function SearchAction(Request $request)
    {
        
		if ($request->isMethod('POST'))
		{
		
		  	$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
			//get paramètre
			$mode = $request->request->get('mode') ;
			$categorie = $request->request->get('categorie') ;
			$depart = $request->request->get('depart') ;
			$arrivee = $request->request->get('arrivee') ;
			$dateDepart = $request->request->get('d-depart') ;
			$dateArrivee = $request->request->get('d-arrivee') ;
			
			$frets = $repository->FretSearch($mode,$categorie,$depart,$arrivee,$dateDepart,$dateArrivee);
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Mode');
		
			$modes = $repository->findAll();
			
			$repository = $this
						->getDoctrine()
						->getManager()
						->getRepository('HellofretBackEndBundle:Mode');
			
			$mode1 = $repository->findBy(array('id' => 1));
			
			
			$repository = $this
						->getDoctrine()
						->getManager()
						->getRepository('HellofretBackEndBundle:CategorieMode');
			
			$categories = $repository->findBy(array('mode' => $mode1));
			
			
			
			return $this->render('HellofretFrontBundle:Fret:search.html.twig', array('frets' => $frets, "modes" => $modes, "categories" => $categories) );
		
		}
		
		return $this->redirectToRoute('hellofret_front_frets');
    }
	
	public function FretViewAction($id, Request $request)
    {
		if ( $this->get('security.context')->isGranted('ROLE_CHARGEUR') ){
			
			return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $id ) );
			
		}elseif( $this->get('security.context')->isGranted('ROLE_TRANSPORTEUR') ){
			return $this->redirectToRoute('hellofret_transporteur_fret_view', array('id' => $id ) );
		}
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Get name of Fret child
		$entityFret = get_class($fret) ;
		$entityName = str_replace("Hellofret\BackEndBundle\Entity\\","",$entityFret);
		/*$contentFret = $repository->getContentFret($id);*/
		
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:ViewFret:view'.$entityName.'.html.twig', array("fret" => $fret ));
    }
	
}
