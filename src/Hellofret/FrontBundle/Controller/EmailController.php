<?php

namespace Hellofret\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmailController extends Controller
{
    public function indexAction()
    {
    }
	
	public function SendMessageAction(Request $request)
    {
		
		if($request->isXmlHttpRequest()) // pour vérifier la présence d'une requete Ajax
        {
            $nom = $request->request->get('nom') ;
			$email = $request->request->get('email') ;
			$sujet = $request->request->get('sujet') ;
			$msg = $request->request->get('message') ;
			
			$txt  = "Vous avez reçu un nouveau message de site hellofret.com \n\n";
			$txt .= "Nom : ".$nom. " \n\n";
			$txt .= "Email : ".$email. " \n\n";
			$txt .= "Sujet : ".$sujet. " \n\n";
			$txt .= "Message : ".$msg. " \n\n";
			
			$message = \Swift_Message::newInstance()
				->setSubject($sujet)
				->setFrom($email)
				->setTo('Youneskidda@gmail.com')
				->setBody($txt,'text/plain');
				
			$this->get('mailer')->send($message);

			$response = new Response();
			$response->setContent("Bien envoyé");
			
			return $response;
        }
		
		return new Response("No Message");
		
    }
	
	public function TestMessageAction(Request $request)
    {
		
			$nom = "Loukmane" ;
			$email = "loukmane@e-i.ma" ;
			$sujet = "test";
			$msg = "Un test" ;
			
			$txt  = "Vous avez reçu un nouveau message de site hellofret.com \n\n";
			$txt .= "Nom : ".$nom. " \n\n";
			$txt .= "Email : ".$email. " \n\n";
			$txt .= "Sujet : ".$sujet. " \n\n";
			$txt .= "Message : ".$msg. " \n\n";
			
			
			$message = \Swift_Message::newInstance()
				->setSubject($sujet)
				->setFrom($email)
				->setTo('loukmane.kech@gmail.com')
				->setBody($txt,'text/plain');
				
			$this->get('mailer')->send($message);

			$response = new Response();
			$response->setContent("c'est bon");
			
			return $response;
		
    }
	
	
}
