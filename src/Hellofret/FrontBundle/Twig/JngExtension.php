<?php
namespace Hellofret\FrontBundle\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;

use Doctrine\ORM\EntityManager;
use Hellofret\UserBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;

class JngExtension extends \Twig_Extension {
    /**
     *
     * @var type 
     */
    protected $request;
    /**
     *
     * @var \Twig_Environment
     */
    protected $environment;
	
	
	private $em;
    private $conn;
	
    /**
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function __construct(Request $request, EntityManager $em, SecurityContext $context) {
        $this->request = $request;
		
		$this->em = $em;
        $this->conn = $em->getConnection();
		$this->context = $context;
    }
	
	public function getUser() {
        return $this->context->getToken()->getUser();
    }
	
	
    /**
     * 
     * @param \Twig_Environment $environment
     */
    public function initRuntime(\Twig_Environment $environment) {
        $this->environment = $environment;
    }
    /**
     * 
     * @return type
     */
    public function getFunctions() {
        return array(
            'get_controller_name' => new \Twig_Function_Method($this, 'getControllerName'),
            'get_action_name' => new \Twig_Function_Method($this, 'getActionName'),
			'get_notification' => new \Twig_Function_Method($this, 'getNotification'),
			'get_count_notification' => new \Twig_Function_Method($this, 'countNotification'),
        );
    }
    
	public function getNotification($type = "new_message") {
		$repository = $this->em
					->getRepository('HellofretBackEndBundle:Notification');
		
		$notifications =  $repository->findNotfication($type, $this->getUser());
		return $notifications;
	}
	
	public function countNotification($type = "new_message", $annonce = NULL) {
		$repository = $this->em
					->getRepository('HellofretBackEndBundle:Notification');
		
		$count =  $repository->countNotif($type, $this->getUser(), $annonce);
		return $count;
	}
	
	
	/**
     * Get current controller name
     */
    public function getControllerName() {
        $pattern = "#Controller\\\([a-zA-Z]*)Controller#";
        $matches = array();
        preg_match($pattern, $this->request->get('_controller'), $matches);
        return strtolower($matches[1]);
    }
    /**
     * Get current action name 
     */
    public function getActionName() {
        $pattern = "#::([a-zA-Z]*)Action#";
        $matches = array();
        preg_match($pattern, $this->request->get('_controller'), $matches);
        return $matches[1];
    }
    /**
     * 
     * @return type
     */
    public function getFilters() {
        return array(
            'diffToDate' => new \Twig_Filter_Method($this, 'diffToDateFilter'),
			'diffToNow' => new \Twig_Filter_Method($this, 'diffToNowFilter'),
			'shortString' => new \Twig_Filter_Method($this, 'shortStringFilter'),
			'videString' => new \Twig_Filter_Method($this, 'videStringFilter'),
        );
    }
	public function videStringFilter($string) {
		if (is_null($string))
            return "Non spécifié" ;
			
		return $string ;
	}
	
	public function shortStringFilter($string) {
		if (is_null($string))
            return;
			
		$chaine = explode(",", $string);
		
		return $chaine[0] ;
	}
	
	public function diffToNowFilter($date1, $unit = "h") {
		
		$date2 = new \Datetime();
		
		if (is_null($date2))
            return;
        $t = $date1->getTimestamp() - $date2->getTimestamp() ;
		
		if( $t <= 86400 ){
			return round($t / 3600, 0) . " h";
		}else{
			return round($t / 86400, 0) . " jours";
		}
		
		
        /*if (is_null($unit)) {
            $unit = "s";
            if ($t >= 3600)
                $unit = "h";
            else if ($t >= 60)
                $unit = "min";
        }
        if ($unit == "s")
            return $t . " s";
        else if ($unit == "min")
            return round($t / 60) . " min";
        else if ($unit == "h")
            return round($t / 3600, 1) . " h";*/
		
	}
	
    /**
     * 
     * @param type $date1
     * @param type $date2
     * @param type $unit
     * @return type
     */
    public function diffToDateFilter($date1, $date2, $unit = null) {
        if (is_null($date2))
            return;
        $t = $date2->getTimestamp() - $date1->getTimestamp();
        if (is_null($unit)) {
            $unit = "s";
            if ($t >= 3600)
                $unit = "h";
            else if ($t >= 60)
                $unit = "min";
        }
        if ($unit == "s")
            return $t . " s";
        else if ($unit == "min")
            return round($t / 60) . " min";
        else if ($unit == "h")
            return round($t / 3600, 1) . " h";
    }
    /**
     * 
     * @return string
     */
    public function getName() {
        return 'jng_extension';
    }
}


