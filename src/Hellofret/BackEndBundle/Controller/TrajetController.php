<?php

namespace Hellofret\BackEndBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Hellofret\BackEndBundle\Entity\Trajet;
use Hellofret\BackEndBundle\Form\TrajetType;
use Hellofret\BackEndBundle\Entity\TrajetArchive;

class TrajetController extends Controller
{
    
	public function addAction(Request $request)
    {
        $trajet  = new Trajet;
		$form = $this->get('form.factory')->create(new TrajetType, $trajet);
		
		if ($form->handleRequest($request)->isValid()) {
			
			$usr= $this->getUser();
			
			$trajet->setProfil($usr);
			
			$em = $this->getDoctrine()->getManager();
     		$em->persist($trajet);
      		$em->flush();
			
			
      		$request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');
			return $this->render('HellofretBackEndBundle:Trajet:sucess-add.html.twig');
			
		}
		
		
		return $this->render('HellofretBackEndBundle:Trajet:add.html.twig', array(
		  'form' => $form->createView(),
		));
		
    }
	
	
	public function editAction($id, Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		
		$form = $this->get('form.factory')->create(new TrajetType, $trajet);
		
		
		if ($form->handleRequest($request)->isValid()) {
			
			$em = $this->getDoctrine()->getManager();
     		$em->persist($trajet);
			
			
      		$em->flush();
			
			
      		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien modifiée');
			return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $id) );
			
		}
		
		
		return $this->render('HellofretBackEndBundle:Trajet:edit.html.twig', array(
		  'form' => $form->createView(),"trajet" => $trajet
		));
		
    }
	
	
	public function saveAction($id, Request $request)
    {
        $user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		if( !$trajet ){
			throw new NotFoundHttpException('Annonce introuvable !');
		}
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:TrajetArchive');
					
		$archive = $repository->findOneBy( array('annonce' => $trajet, 'profil' => $user) );
		
		
		if( $archive ){
			$request->getSession()->getFlashBag()->add('notice', 'Cette annonce est déjà enregistré !');
			
			if ( $user->hasRole('ROLE_CHARGEUR') ){
				
				return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $id) );
				
			}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
				
				return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $id) );
				
			}
		}
		
		$archive = new TrajetArchive ;
		$archive->setAnnonce($trajet);
		$archive->setProfil($user);
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($archive);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien sauvegardé');
		
		
		if ( $user->hasRole('ROLE_CHARGEUR') ){
			return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $id) );
			
		}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
			return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $id) );
			
		}
		
		
    }
	
	
	public function deleteTrajetAction($id, Request $request)
    {
        
		$profile = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id, "profil" => $profile ));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$em->remove($trajet);
		$em->flush();
		
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce à bien été supprimé !');
		return $this->redirectToRoute('hellofret_transporteur_trajet_encours');
    }
	
}
