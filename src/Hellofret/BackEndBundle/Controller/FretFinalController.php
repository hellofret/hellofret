<?php

namespace Hellofret\BackEndBundle\Controller;

use Symfony\Component\Validator\Constraints\DateTime;

use Hellofret\BackEndBundle\Entity\FretAlimentation;
use Hellofret\BackEndBundle\Entity\FretAnimaux;
use Hellofret\BackEndBundle\Entity\FretBoisSable;
use Hellofret\BackEndBundle\Entity\FretCerealEpice;
use Hellofret\BackEndBundle\Entity\FretConteneur;
use Hellofret\BackEndBundle\Entity\FretDechet;
use Hellofret\BackEndBundle\Entity\FretHydrocarbure;
use Hellofret\BackEndBundle\Entity\FretConditionne;
use Hellofret\BackEndBundle\Entity\FretPalettes;
use Hellofret\BackEndBundle\Entity\FretVehicule;
use Hellofret\BackEndBundle\Entity\FretDemenagement;
use Hellofret\BackEndBundle\Entity\FretEquipement;
use Hellofret\BackEndBundle\Entity\FretMessagerie;
use Hellofret\BackEndBundle\Entity\FretPharmacie;

use Hellofret\BackEndBundle\Form\FretAlimentationType;
use Hellofret\BackEndBundle\Form\FretAnimauxType;
use Hellofret\BackEndBundle\Form\FretBoisSableType;
use Hellofret\BackEndBundle\Form\FretCerealEpiceType;
use Hellofret\BackEndBundle\Form\FretConteneurType;
use Hellofret\BackEndBundle\Form\FretDechetType;
use Hellofret\BackEndBundle\Form\FretHydrocarbureType;
use Hellofret\BackEndBundle\Form\FretConditionneType;
use Hellofret\BackEndBundle\Form\FretPalettesType;
use Hellofret\BackEndBundle\Form\FretVehiculeType;
use Hellofret\BackEndBundle\Form\FretDemenagementType;
use Hellofret\BackEndBundle\Form\FretEquipementType;
use Hellofret\BackEndBundle\Form\FretMessagerieType;
use Hellofret\BackEndBundle\Form\FretPharmacieType;

use Hellofret\BackEndBundle\Entity\Fret;
use Hellofret\BackEndBundle\Entity\TypeFret;
use Hellofret\BackEndBundle\Form\FretType;
use Hellofret\BackEndBundle\Form\TypeFretType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;








class FretFinalController extends Controller
{
    public function addAction($fret, Request $request)
    {
		
		$fretE = 'Hellofret\BackEndBundle\Entity\\'.$fret ;
		$childFret  = new $fretE;
		
		$formT = 'Hellofret\BackEndBundle\Form\\'.$fret.'Type' ;
		$formType = new $formT ;
		
		
		$form = $this->get('form.factory')->create(new $formType, $childFret);
        
		if ($form->handleRequest($request)->isValid()) {
			
			$usr= $this->getUser();
			$childFret->setProfil($usr);
			
			
			$em = $this->getDoctrine()->getManager();
     		$em->persist($childFret);
      		$em->flush();
			
			
      		$request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistrée.');
			return $this->render('HellofretBackEndBundle:Fret:add_succes.html.twig', array('slug' => strtolower($fret) ) );
			
			
			//var_dump( $childFret );die();
		}
		
		
		$response = new Response();
		$response->setContent( "Un problème est survenu lors de soumission de votre annonce" );
		return $response;
		
    }
	
	
}
