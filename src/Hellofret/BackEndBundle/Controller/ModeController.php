<?php

namespace Hellofret\BackEndBundle\Controller;

use Hellofret\BackEndBundle\Entity\Mode;
use Hellofret\BackEndBundle\Entity\CategorieMode;
use Hellofret\BackEndBundle\Form\ModeType;
use Hellofret\BackEndBundle\Form\CategorieModeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class ModeController extends Controller
{
    public function createAction(Request $request)
    {
        
		$mode = new Mode;

		$form = $this->get('form.factory')->create(new modeType, $mode);
		
		if ($form->handleRequest($request)->isValid()) {
			
			
		  $em = $this->getDoctrine()->getManager();
		  $em->persist($mode);
		  $em->flush();
		  $request->getSession()->getFlashBag()->add('notice', 'Mode bien enregistré.');
		  return $this->redirect($this->generateUrl('hellofret_mode_add'));
		}
		return $this->render('HellofretBackEndBundle:Mode:create.html.twig', array(
		  'form' => $form->createView(),
		));
		
    }
	public function createCategorieAction(Request $request)
    {
        
		$categorie = new CategorieMode;

		$form = $this->get('form.factory')->create(new CategorieModeType, $categorie);
		
		if ($form->handleRequest($request)->isValid()) {
			
			
		  $em = $this->getDoctrine()->getManager();
		  $em->persist($categorie);
		  $em->flush();
		  $request->getSession()->getFlashBag()->add('notice', 'Categorie Mode bien enregistré.');
		  return $this->redirect($this->generateUrl('hellofret_mode_categorie_add'));
		}
		return $this->render('HellofretBackEndBundle:Mode:createCategorie.html.twig', array(
		  'form' => $form->createView(),
		));
		
    }
	
	
}
