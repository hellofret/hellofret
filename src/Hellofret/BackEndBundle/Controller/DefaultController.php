<?php

namespace Hellofret\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('HellofretBackEndBundle:Default:index.html.twig');
    }
}
