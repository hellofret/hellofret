<?php

namespace Hellofret\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Hellofret\BackEndBundle\Entity\Notification;

class NotificationController extends Controller
{
	
	public function UpdateAction(Request $request)
    {
		
		if($request->isXmlHttpRequest()) // pour v�rifier la pr�sence d'une requete Ajax
        {
            $idnotif = $request->request->get('idnotif') ;
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Notification');
					
			$notification = $repository->findOneBy(array('id' => $idnotif));
			
			if( !$notification ){
				throw new NotFoundHttpException('Page introuvable !');
			}
			
			$notification->setEtat(true);
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($notification);
			
			$em->flush();

			$response = new Response();
			$response->setContent("Vu");
			
			return $response;
        }
		
		return new Response("No Notification");
		
    }
	
	
	public function UpdateByAnnonceAction(Request $request)
    {
		
		if($request->isXmlHttpRequest()) // pour v�rifier la pr�sence d'une requete Ajax
        {
            $annonce = $request->request->get('idannonce') ;
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Notification');
					
			$notifications = $repository->findBy(array('annonce' => $annonce));
			
			$em = $this->getDoctrine()->getManager();
			foreach( $notifications as $notification ){
				$notification->setEtat(true);
				$em->persist($notification);
			}
			
			$em->flush();

			$response = new Response();
			$response->setContent("Vu");
			
			return $response;
        }
		
		return new Response("No Notification");
		
    }
	
	public function redirectNotificationAction($id, Request $request)
    {
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Notification');
					
		$notification = $repository->findOneBy(array('id' => $id));
		
		if( !$notification ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$notification->setEtat(true);
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($notification);
		
		$em->flush();
		
		$ancre ="";
		if( $notification->getType() == "new_message" ){
			$ancre ="#messages";
		}else if( $notification->getType() != "annonce_approuver" || $notification->getType() != "edit_annonce" ) {
			$ancre ="#devis";
		}
		
		return $this->redirect( $notification->getUrl().$ancre );
		
    }
	
	
	public function getNewNotificationAction(Request $request)
    {
        
		if($request->isXmlHttpRequest()) 
        {
            
			$user = $request->request->get('profil') ;
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:User');
					
			$user = $repository->findOneBy(array('id' => $user));
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Notification');
					
			$notifications = $repository->findBy(array('profil' => $user, 'showed' => false));
			
			$output = array();
			$i = 0;
			$em = $this->getDoctrine()->getManager();
      		
			foreach( $notifications as $notification ){
				
				//prepare to raz 
				$notification->setRazed(true) ;
				$em->persist($notification);
				
				$output[$i]["id"] = $notification->getId() ;
				$output[$i]["type"] = $notification->getType() ;
				$output[$i]["title"] = $notification->getPrioritaire()->getUsername() ;
				$output[$i]["message"] = $notification->getTitre() ;
				$output[$i]["url"] = $this->generateUrl('hellofret_notification_redirect',array('id' => $notification->getId()));
				if( $notification->getPrioritaire()->getId() == 1 ){
					$output[$i]["image"] = "images/logo.png" ;
				}else{
					$output[$i]["image"] = $notification->getPrioritaire()->getProfile()->getLogo()->getWebPath() ;
				}
				
				$i += 1 ;
			}
			
			$em->flush();
			
			
			$response = new Response();
			$output = json_encode( $output );
			$response->headers->set('Content-Type', 'application/json');
			$response->setContent($output);
			
			
			
			return $response;
        }
		
		return new Response("Erreur, Pas de notification !");
		
    }
	
	public function RazNotificationAction(Request $request)
    {
		
		if($request->isXmlHttpRequest()) // pour v�rifier la pr�sence d'une requete Ajax
        {
            $user = $this->getUser();
			
			//Mise � jour des notif
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Notification');
					
			$action = $repository->setShowedNotification($user, true);
			
			$response = new Response();
			$response->setContent("Raz");
			
			return $response;
        }
		
		return new Response("No Raz");
		
    }
	
	
}
