<?php

namespace Hellofret\BackEndBundle\Controller;

use Hellofret\BackEndBundle\Entity\Fret;
use Hellofret\BackEndBundle\Entity\FretArchive;
use Hellofret\BackEndBundle\Entity\TypeFret;
use Hellofret\BackEndBundle\Form\FretType;
use Hellofret\BackEndBundle\Form\TypeFretType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Hellofret\BackEndBundle\Entity\Devis;
use Hellofret\BackEndBundle\Entity\Notification;


use Hellofret\BackEndBundle\Entity\FretAlimentation;
use Hellofret\BackEndBundle\Entity\FretAnimaux;
use Hellofret\BackEndBundle\Entity\FretBoisSable;
use Hellofret\BackEndBundle\Entity\FretCerealEpice;
use Hellofret\BackEndBundle\Entity\FretConteneur;
use Hellofret\BackEndBundle\Entity\FretDechet;
use Hellofret\BackEndBundle\Entity\FretHydrocarbure;
use Hellofret\BackEndBundle\Entity\FretConditionne;
use Hellofret\BackEndBundle\Entity\FretPalettes;
use Hellofret\BackEndBundle\Entity\FretVehicule;
use Hellofret\BackEndBundle\Entity\FretDemenagement;
use Hellofret\BackEndBundle\Entity\FretEquipement;
use Hellofret\BackEndBundle\Entity\FretMessagerie;
use Hellofret\BackEndBundle\Entity\FretPharmacie;


use Hellofret\BackEndBundle\Form\FretAlimentationType;
use Hellofret\BackEndBundle\Form\FretAnimauxType;
use Hellofret\BackEndBundle\Form\FretBoisSableType;
use Hellofret\BackEndBundle\Form\FretCerealEpiceType;
use Hellofret\BackEndBundle\Form\FretConteneurType;
use Hellofret\BackEndBundle\Form\FretDechetType;
use Hellofret\BackEndBundle\Form\FretHydrocarbureType;
use Hellofret\BackEndBundle\Form\FretConditionneType;
use Hellofret\BackEndBundle\Form\FretPalettesType;
use Hellofret\BackEndBundle\Form\FretVehiculeType;
use Hellofret\BackEndBundle\Form\FretDemenagementType;
use Hellofret\BackEndBundle\Form\FretEquipementType;
use Hellofret\BackEndBundle\Form\FretMessagerieType;
use Hellofret\BackEndBundle\Form\FretPharmacieType;


class FretController extends Controller
{
    public function addAction(Request $request)
    {
        
		$fret  = new Fret;
		$form = $this->get('form.factory')->create(new FretType, $fret);
		
		if ($form->handleRequest($request)->isValid()) {
			
			$typeFret = $fret->getCategorie()->getType()->getTitre() ;
			//$view = strtolower($typeFret) ;
			
			
			
			$fretE = 'Hellofret\BackEndBundle\Entity\\'.$typeFret ;
			$childFret  = new $fretE;
			
			$formT = 'Hellofret\BackEndBundle\Form\\'.$typeFret.'Type' ;
			$formType = new $formT ;
			
			
			$childFret->setTitre($fret->getTitre());
			$childFret->setMode($fret->getMode());
			$childFret->setCategorie($fret->getCategorie());
			$childFret->setVilleDepart($fret->getVilleDepart());
			$childFret->setDateDepart($fret->getDateDepart());
			$childFret->setVilleArrivee($fret->getVilleArrivee());
			$childFret->setDateArrivee($fret->getDateArrivee());
			
			$childFret->setDateDepartFlexible($fret->getDateDepartFlexible());
			$childFret->setDateArriveeFlexible($fret->getDateArriveeFlexible());
			$childFret->setDepartIsFlexible($fret->getDepartIsFlexible());
			$childFret->setArriveIsFlexible($fret->getArriveIsFlexible());
			
			
			
			$form = $this->get('form.factory')->create($formType, $childFret);
			
			//var_dump( $formType,$childFret );die();
		 	
			return $this->render('HellofretBackEndBundle:Fret:add'.$typeFret.'.html.twig', array(
		  			'form' => $form->createView(), 'slug' => strtolower($typeFret),
			));
		}
		
		
		
		return $this->render('HellofretBackEndBundle:Fret:add.html.twig', array(
		  'form' => $form->createView(),
		));
		
    }
	
	public function editAction($id, Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$typeFret = $fret->getCategorie()->getType()->getTitre() ;
			
			
		/*$fretE = 'Hellofret\BackEndBundle\Entity\\'.$typeFret ;
		$childFret  = new $fretE;*/
		
		$formT = 'Hellofret\BackEndBundle\Form\\'.$typeFret.'Type' ;
		$formType = new $formT ;
		
		//var_dump( $childFret );die();
		
		$form = $this->get('form.factory')->create($formType, $fret);
		
		if ($form->handleRequest($request)->isValid()) {
			
			
			$form = $this->get('form.factory')->create($formType, $fret);
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($fret);
			
			
			//retourner les tronsporteur en question
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		
			$devis =  $repository->findBy(array('annonce' => $fret));
			$ithems = array();
			
			foreach( $devis as $devi ){
				//set attribut of Notification
				$notfication  = new Notification;
				$notfication->setTitre("Le chargeur à modifier l'annonce sous titre : ".$fret->getTitre());
				$url = $this->generateUrl('hellofret_transporteur_fret_view', array('id' => $fret->getId()) );
				$notfication->setUrl($url);
				$notfication->setType("edit_annonce");
				$notfication->setProfil($devi->getProfil());
				$notfication->setPrioritaire($this->getUser());
				$notfication->setAnnonce($fret->getId());
				
				$ithems[] = $notfication;
			}
			
			$em = $this->getDoctrine()->getManager();
      		foreach( $ithems as $ithem ){
				$em->persist($ithem);
			}
			
			
      		$em->flush();
		 	
			$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien modifié');
			return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $id) );
		}
		
		
		
		return $this->render('HellofretBackEndBundle:Fret:edit'.$typeFret.'.html.twig', array(
		  			'form' => $form->createView(), 'slug' => strtolower($typeFret),
		));
		
    }
	
	public function saveAction($id, Request $request)
    {
        $user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Annonce introuvable !');
		}
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:FretArchive');
					
		$archive = $repository->findOneBy( array('annonce' => $fret, 'profil' => $user) );
		
		
		if( $archive ){
			$request->getSession()->getFlashBag()->add('notice', 'Cette annonce est déjà enregistré !');
			
			if ( $user->hasRole('ROLE_CHARGEUR') ){
				
				return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $id) );
				
			}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
				
				return $this->redirectToRoute('hellofret_transporteur_fret_view', array('id' => $id) );
				
			}
		}
		
		$archive = new FretArchive ;
		$archive->setAnnonce($fret);
		$archive->setProfil($user);
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($archive);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien sauvegardé');
		
		
		if ( $user->hasRole('ROLE_CHARGEUR') ){
			return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $id) );
			
		}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
			return $this->redirectToRoute('hellofret_transporteur_fret_view', array('id' => $id) );
			
		}
		
		
		//var_dump( $childFret );die();
		
    }
	
	public function boosterAction($id, Request $request)
    {
        $user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id, 'profil' => $user));
		
		if( !$fret ){
			throw new NotFoundHttpException('Annonce introuvable !');
		}
		
		$fret->setDateCreation( new \Datetime() );
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($fret);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien boosté !');
		if ( $user->hasRole('ROLE_CHARGEUR') ){
			return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $id) );
			
		}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
			return $this->redirectToRoute('hellofret_transporteur_fret_view', array('id' => $id) );
			
		}
		
		//var_dump( $childFret );die();
		
    }
	
	public function deleteFretAction($id, Request $request)
    {
        
		$profile = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id, "profil" => $profile ));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		
		$em = $this->getDoctrine()->getEntityManager();
		$em->remove($fret);
		$em->flush();
		
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce à bien été supprimé !');
		return $this->redirectToRoute('hellofret_chargeur_fret_cours');
    }
	
	public function createTypeAction(Request $request)
    {
        
		$typefret = new TypeFret;

		$form = $this->get('form.factory')->create(new TypeFretType, $typefret);
		
		if ($form->handleRequest($request)->isValid()) {
			
			
		  $em = $this->getDoctrine()->getManager();
		  $em->persist($typefret);
		  $em->flush();
		  $request->getSession()->getFlashBag()->add('notice', 'Type fret bien enregistré.');
		  return $this->redirect($this->generateUrl('hellofret_fret_type_add'));
		}
		return $this->render('HellofretBackEndBundle:Fret:createType.html.twig', array(
		  'form' => $form->createView(),
		));
		
    }
	public function findAction(Request $request)
    {
		if($request->isXmlHttpRequest()) // pour vérifier la présence d'une requete Ajax
        {
            $mode = $request->request->get('mode') ;
			
			if(!$mode){
				$mode = 1;
			}
			//Pour utiliser les même catégorie de Mode 1
			if($mode == 2){
				$mode = 1;
			}
			$listCategorie = $this
				->getDoctrine()
				->getManager()
				->getRepository('HellofretBackEndBundle:CategorieMode')
				->getCategorieByMode($mode)
			 ;
			 
			$listCategorie = $listCategorie->getQuery()->getResult();
			$categories = array();
			$count = 0 ;
			foreach ($listCategorie as $Categorie) {
				$categories[$count]["id"] = $Categorie->getId() ;
				$categories[$count]["titre"] = $Categorie->getTitre() ;
				$count += 1 ;
			}
			
			$response = new Response();
			$categories = json_encode( $categories );
			$response->headers->set('Content-Type', 'application/json');
			$response->setContent($categories);
			return $response;
        }
		
		return new Response("No catégorie");
	}
	
	public function viewAction()
    {
		
		$fret = new Fret;
		
		return $this->render('HellofretBackEndBundle:Fret:view.html.twig');
	}
}
