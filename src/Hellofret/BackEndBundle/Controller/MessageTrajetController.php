<?php

namespace Hellofret\BackEndBundle\Controller;

use Hellofret\BackEndBundle\Entity\Trajet;
use Hellofret\BackEndBundle\Entity\MessageTrajet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Hellofret\BackEndBundle\Entity\Notification;




class MessageTrajetController extends Controller
{
    
	public function addAction(Request $request)
    {
        
		if($request->isXmlHttpRequest()) 
        {
            $fret = $request->request->get('fret') ;
			$content = $request->request->get('content') ;
			$user = $request->request->get('profil') ;
			
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
			$fret = $repository->findOneBy(array('id' => $fret));
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:User');
					
			$user = $repository->findOneBy(array('id' => $user));
			
			
			$message = new MessageTrajet;
			$message->setContent($content);
			$message->setAnnonce($fret);
			$message->setProfil($user);
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($message);
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:MessageTrajet');
		
			$messages =  $repository->getMessagesDistinct( $fret, $user->getId() );
			
			
			$ithems = array();
			$ids = array();
			foreach( $messages as $msg ){
				//set attribut of Notification
				$notfication  = new Notification;
				$notfication->setTitre("Un nouveau message a été publié dans l'annonce : ".$fret->getTitre());
				
				if( $msg->getProfil()->hasRole("ROLE_CHARGEUR") ){
					$url = $this->generateUrl('hellofret_chargeur_trajet_view', array('id' => $fret->getId()) );
				}else{
					$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $fret->getId()) );
				}
				
				$notfication->setUrl($url);
				$notfication->setType("new_message");
				$notfication->setProfil($msg->getProfil());
				$notfication->setPrioritaire($this->getUser());
				$notfication->setAnnonce($fret->getId());
				
				$ithems[] = $notfication;
				$ids[] = $msg->getProfil()->getId();
			}
			
			
			if( !in_array($fret->getProfil()->getId(),$ids) ){
				$notfication  = new Notification;
				$notfication->setTitre("Un nouveau message a été publié dans l'annonce : ".$fret->getTitre());
				if( $fret->getProfil()->hasRole("ROLE_CHARGEUR") ){
					$url = $this->generateUrl('hellofret_chargeur_trajet_view', array('id' => $fret->getId()) );
				}else{
					$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $fret->getId()) );
				}
				$notfication->setUrl($url);
				$notfication->setType("new_message");
				$notfication->setProfil($fret->getProfil());
				$notfication->setPrioritaire($this->getUser());
				$notfication->setAnnonce($fret->getId());
				$em->persist($notfication);
			}
			
			
			
      		foreach( $ithems as $ithem ){
				$em->persist($ithem);
			}
			
			
      		$em->flush();
			
			
			$output = array();
			$output[0]["id"] = $message->getId() ;
			
			$output[0]["date"] = date_format($message->getDate(),'d/m/Y H:i:s');
			
			
			$response = new Response();
			$output = json_encode( $output );
			$response->headers->set('Content-Type', 'application/json');
			$response->setContent($output);
			return $response;
        }
		
		
		
		return new Response("Erreur, le message n'a pas été envoyé !");
		
    }
	
	
}
