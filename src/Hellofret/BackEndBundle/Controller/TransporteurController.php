<?php

namespace Hellofret\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

use Hellofret\UserBundle\Entity\Profile;
use Hellofret\UserBundle\Form\ModificationProfileType;
use Hellofret\BackEndBundle\Entity\Devis;
use Hellofret\BackEndBundle\Entity\Notification;
use Hellofret\BackEndBundle\Form\DevisType;
use Hellofret\BackEndBundle\Form\DevisDemandeType;
use Hellofret\BackEndBundle\Entity\DevisDemande;
use Hellofret\BackEndBundle\Entity\VoitureLocation;
use Hellofret\BackEndBundle\Form\VoitureLocationType;
use Hellofret\BackEndBundle\Entity\VoitureArchive;

class TransporteurController extends Controller
{
    public function indexAction(Request $request)
    {
        
		$profil = $this->getUser()->getProfile() ;
		
		$form = $this->get('form.factory')->create(new ModificationProfileType, $profil);
		$form->remove('metier');
		
		if ($form->handleRequest($request)->isValid()) {
			
			// c'est elle qui déplace l'image là où on veut les stocker
      		$profil->getLogo()->upload();
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($profil);
      		$em->flush();


      		$request->getSession()->getFlashBag()->add('notice', 'Informations bien enregistrée.');
			return $this->render('HellofretBackEndBundle:Transporteur:index.html.twig', array(
						'form' => $form->createView(), 'profil' => $profil,
			));
			
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:index.html.twig', array(
		  			'form' => $form->createView(), 'profil' => $profil,
		));
    }
	
	public function FretDisponibleAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$listFretEnCours = $repository->FretDisponible() ;	
		
						
		//var_dump( $listFretEnCours[0]->getId() );die();
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:fretDisponible.html.twig', array("frets" => $listFretEnCours));
    }
	
	public function FretCibleAction(Request $request)
    {
        
		$user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		
		$devis = $repository->findBy(array('profil' => $user));
		
		$fretsin = array();
		if( $devis ){
			foreach( $devis as $devi ){
				$fretsin[] = $devi->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$listFretEnCours = $repository->FretsCible($fretsin) ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:fretEncours.html.twig', array("frets" => $listFretEnCours));
    }
	
	public function FretsSauvgarderAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:FretArchive');
		
		$listFretsSauvgarder = $repository->Allarchive($user);
		
		
		$ithems = array();
		
		if($listFretsSauvgarder ){
			foreach( $listFretsSauvgarder as $fret ){
				$ithems[] = $fret->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$fretsSauvgarder = $repository->FretEnregistree($ithems) ;	
		
						
		//var_dump( $listFretEnCours[0]->getId() );die();
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:fretSauvgarder.html.twig', array("frets" => $fretsSauvgarder));
    }
	
	
	public function FretViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Get name of Fret child
		$entityFret = get_class($fret) ;
		$entityName = str_replace("Hellofret\BackEndBundle\Entity\\","",$entityFret);
		/*$contentFret = $repository->getContentFret($id);*/
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		$devis =  $repository->findBy(array('profil' => $this->getUser(), 'annonce' => $fret));
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Message');
		$messages =  $repository->findBy(array('annonce' => $fret));
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:ViewFret:view'.$entityName.'.html.twig', array("fret" => $fret, "devis" => $devis, "messages" => $messages ));
    }
	public function DevisAddAction($id, Request $request)
    {
        $user = $this->getUser();
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
					
		$devis = $repository->findOneBy(array('profil' => $user, "annonce" => $fret ));
		
		
		if( (!$fret) or ($devis) ){
			throw new NotFoundHttpException('Vous pourrez pas faire cette action !');
		}
		
		
		$devis  = new Devis;
		$form = $this->get('form.factory')->create(new DevisType, $devis);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			//set attribut of Devis
			$usr= $this->getUser();
			$devis->setProfil($usr);
			$devis->setAnnonce($fret);
			
			
			//set attribut of Notification
			$notfication  = new Notification;
			$notfication->setTitre("Vous avez reçu un nouveau devis de ton annonce :".$fret->getTitre());
			$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
			$notfication->setUrl($url);
			$notfication->setType("new_devis");
			$notfication->setProfil($fret->getProfil());
			$notfication->setPrioritaire($this->getUser());
			$notfication->setAnnonce($fret->getId());
			
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($devis);
			$em->persist($notfication);
			
			
      		$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre devis est bien envoyé.');
			return $this->redirectToRoute('hellofret_transporteur_fret_view', array('id' => $fret->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Devis:add.html.twig', array("form" => $form->createView(), "fret" => $fret ));
    }
	public function DevisEditAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		
		$devis = $repository->getDevis($id);
		
		if( !$devis ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$fret = $devis->getAnnonce();
		$form = $this->get('form.factory')->create(new DevisType, $devis);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			//Incrémenter le count de modification des devis pas plus de 2
			$devis->setRepetition(1);
			$devis->setApprobation(NULL);
			
			//set attribut of Notification
			$notfication  = new Notification;
			$notfication->setTitre("Le transporteur ".$devis->getProfil()->getProfile()->getNomCommercial()." a modifié son propre devis de ton annnonce :".$fret->getTitre());
			$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
			$notfication->setUrl($url);
			$notfication->setType("edit_devis");
			$notfication->setProfil($fret->getProfil());
			$notfication->setPrioritaire($this->getUser());
			$notfication->setAnnonce($fret->getId());
			
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($devis);
			$em->persist($notfication);
			
			
      		$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre devis est bien modifié.');
			return $this->redirectToRoute('hellofret_transporteur_fret_view', array('id' => $fret->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Devis:edit.html.twig', array("form" => $form->createView(), "fret" => $fret ));
    }
	
	public function TrajetsEnCoursAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$listTrajetsEnCours = $repository->trajetsEnCours($user) ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:trajetsEnCours.html.twig', array("trajets" => $listTrajetsEnCours));
    }
	
	public function RetoursVideAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$listTrajetsEnCours = $repository->findBy(array('profil' => $user, 'type' => "Retour à vide") );
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:trajetsEnCours.html.twig', array("trajets" => $listTrajetsEnCours));
    }
	
	public function TrajetViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		if( !$trajet ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:MessageTrajet');
		
		$messages =  $repository->findBy(array('annonce' => $trajet));
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
					
		$demande = $repository->findBy(array("annonce" => $trajet ));
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Trajet:viewTrajet.html.twig', array("trajet" => $trajet, "messages" => $messages, "demandes"=> $demande));
    }
	
	public function TrajetSauvgarderAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:TrajetArchive');
		
		$listFretsSauvgarder = $repository->Allarchive($user);
		
		
		$ithems = array();
		
		if($listFretsSauvgarder ){
			foreach( $listFretsSauvgarder as $fret ){
				$ithems[] = $fret->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$trajetsSauvgarder = $repository->TrajetsEnregistree($ithems) ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:trajetSauvgarder.html.twig', array("trajets" => $trajetsSauvgarder));
    }
	
	public function TrajetboosterAction($id, Request $request)
    {
		$user = $this->getUser();
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('profil' => $user,'id' => $id));
		
		if( !$trajet ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$trajet->setDateCreation( new \Datetime() );
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($trajet);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien boosté !');
		return $this->redirectToRoute('hellofret_transporteur_trajet_encours' );
    }
	
	public function RefuserDemandeAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
		
		$demande = $repository->findOneBy(array('id' => $id));
		
		if( !$demande ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		$trajet = $demande->getAnnonce();
		$demande->setApprobation(false);
		
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("Le transporteur ".$trajet->getProfil()->getProfile()->getNomCommercial()." a refusé ta demande sur son annonce :".$trajet->getTitre());
		$url = $this->generateUrl('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("demande_refuser");
		$notfication->setProfil($demande->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($trajet->getId());
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($demande);
		$em->persist($notfication);
		
		
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'La demande est bien refusé !');
		return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
			
			
    }
	
	
	public function DevisDemandeAddAction($id, Request $request)
    {
        $user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
					
		$demande = $repository->findOneBy(array('id' => $id));
		
		if( (!$demande) or ($demande->getDevisDemande()) ){
			throw new NotFoundHttpException('Vous pourrez pas faire cette action !');
		}
		
		$trajet = $demande->getAnnonce() ;
		
		$devis  = new DevisDemande;
		$form = $this->get('form.factory')->create(new DevisDemandeType, $devis);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			//set attribut of Devis
			$user = $this->getUser();
			$devis->setProfil($user);
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($devis);
			$em->flush();
			
			$demande->setDevisDemande($devis) ;
			$em->persist($demande);
			
			//set attribut of Notification
			$notfication  = new Notification;
			$notfication->setTitre("Vous avez reçu un nouveau devis sur ta demande de l'annonce :".$trajet->getTitre());
			$url = $this->generateUrl('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
			$notfication->setUrl($url);
			$notfication->setType("new_devis");
			$notfication->setProfil($demande->getProfil());
			$notfication->setPrioritaire($this->getUser());
			$notfication->setAnnonce($trajet->getId());
			
			$em->persist($notfication);
			
			
      		$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre devis est bien envoyé.');
			return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:DevisDemande:add.html.twig', array("form" => $form->createView(), "trajet" => $trajet ));
    }
	
	public function DevisDemandeEditAction($id, Request $request)
    {
        $user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:DevisDemande');
					
		$devis = $repository->findOneBy(array('id' => $id, "profil" => $user));
		
		if( !$devis ){
			throw new NotFoundHttpException('Vous pourrez pas faire cette action !');
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
		$demande = $repository->findOneBy(array('devisDemande' => $devis->getId()));
		$trajet = $demande->getAnnonce() ;
		
		$form = $this->get('form.factory')->create(new DevisDemandeType, $devis);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			//set attribut of Devis
			$user = $this->getUser();
			
			//Incrémenter le count de modification des devis pas plus de 2
			$devis->setRepetition(1);
			$devis->setApprobation(NULL);
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($devis);
			
			//set attribut of Notification
			$notfication  = new Notification;
			$notfication->setTitre("Le transporteur ".$devis->getProfil()->getProfile()->getNomCommercial()." a modifié son devis sur ta demande de l'annonce :".$trajet->getTitre());
			$url = $this->generateUrl('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
			$notfication->setUrl($url);
			$notfication->setType("edit_devis");
			$notfication->setProfil($demande->getProfil());
			$notfication->setAnnonce($trajet->getId());
			
			$em->persist($notfication);
			
			
      		$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre devis est bien modifié.');
			return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:DevisDemande:edit.html.twig', array("form" => $form->createView(), "trajet" => $trajet ));
    }
	
	public function VoitureLocationAddAction(Request $request)
    {
        
		
		$voiture = new VoitureLocation ;
		
		$form = $this->get('form.factory')->create(new VoitureLocationType, $voiture);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			// c'est elle qui déplace l'image là où on veut les stocker
      		$images = $voiture->getPhotos();
			foreach( $images as $image){
				$image->upload();
			}
			
			$user = $this->getUser();
			$voiture->setProfil($user);
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($voiture);
			$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien publié.');
			return $this->render('HellofretBackEndBundle:Location:addsucces.html.twig', array("form" => $form->createView() ));
			//return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Location:add.html.twig', array("form" => $form->createView() ));
    }
	
	public function MesVoituresLocationAction(Request $request)
    {
        $user = $this->getUser();
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
		$voitures = $repository->findBy(array('profil' => $user ));
			
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:mesVoiture.html.twig', array("voitures" => $voitures));
    }
	
	public function VoitureViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Location:viewVoiture.html.twig', array("voiture" => $voiture));
    }
	public function VoitureEditAction($id, Request $request)
    {
		$user = $this->getUser();
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('profil' => $user,'id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$form = $this->get('form.factory')->create(new VoitureLocationType, $voiture);
		$form->remove("photos");
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			// c'est elle qui déplace l'image là où on veut les stocker
      		$images = $voiture->getPhotos();
			foreach( $images as $image){
				$image->upload();
			}
			
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($voiture);
			$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien modifié !');
			return $this->redirectToRoute('hellofret_transporteur_voiture_view', array('id' => $voiture->getId()) );
			//return $this->redirectToRoute('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
		}
		
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Location:edit.html.twig', array("form" => $form->createView() ));
    }
	
	public function VoitureDeleteAction($id, Request $request)
    {
		$user = $this->getUser();
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('profil' => $user,'id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($voiture);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien Supprimée !');
		return $this->redirectToRoute('hellofret_transporteur_mesvoiture' );
    }
	
	public function VoitureBoosterAction($id, Request $request)
    {
		$user = $this->getUser();
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('profil' => $user,'id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$voiture->setDateCreation( new \Datetime() );
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($voiture);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien boosté !');
		return $this->redirectToRoute('hellofret_transporteur_mesvoiture' );
    }
	
	public function saveVoitureAction($id, Request $request)
    {
        $user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Annonce introuvable !');
		}
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureArchive');
					
		$archive = $repository->findOneBy( array('annonce' => $voiture, 'profil' => $user) );
		
		
		if( $archive ){
			$request->getSession()->getFlashBag()->add('notice', 'Cette annonce est déjà enregistré !');
			
			if ( $user->hasRole('ROLE_CHARGEUR') ){
				
				return $this->redirectToRoute('hellofret_chargeur_voiture_view', array('id' => $id) );
				
			}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
				
				return $this->redirectToRoute('hellofret_transporteur_voiture_view', array('id' => $id) );
				
			}
		}
		
		$archive = new VoitureArchive ;
		$archive->setAnnonce($voiture);
		$archive->setProfil($user);
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($archive);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Votre annonce est bien sauvegardée');
		
		
		if ( $user->hasRole('ROLE_CHARGEUR') ){
			return $this->redirectToRoute('hellofret_chargeur_voiture_view', array('id' => $id) );
			
		}elseif ( $user->hasRole('ROLE_TRANSPORTEUR') ){
			return $this->redirectToRoute('hellofret_transporteur_voiture_view', array('id' => $id) );
			
		}
		
		
    }
	
	public function VoituresSauvgarderAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureArchive');
		
		$voituresSauvgarder = $repository->Allarchive($user);
		
		
		$ithems = array();
		
		if($voituresSauvgarder ){
			foreach( $voituresSauvgarder as $voiture ){
				$ithems[] = $voiture->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
		$voituresSauvgarder = $repository->VoituresEnregistree($ithems) ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Transporteur:voituresSauvgarder.html.twig', array("voitures" => $voituresSauvgarder));
    }
	
	
}
