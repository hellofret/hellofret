<?php

namespace Hellofret\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Hellofret\BackEndBundle\Entity\Notification;
use Hellofret\BackEndBundle\Entity\Demande;
use Hellofret\BackEndBundle\Form\DemandeType;

use Hellofret\BackEndBundle\Entity\Trajet;
use Hellofret\UserBundle\Entity\Profile;
use Hellofret\UserBundle\Form\ModificationProfileType;

use Hellofret\BackEndBundle\Entity\FretAlimentation;
use Hellofret\BackEndBundle\Entity\FretAnimaux;
use Hellofret\BackEndBundle\Entity\FretBoisSable;
use Hellofret\BackEndBundle\Entity\FretCerealEpice;
use Hellofret\BackEndBundle\Entity\FretConteneur;
use Hellofret\BackEndBundle\Entity\FretDechet;
use Hellofret\BackEndBundle\Entity\FretHydrocarbure;
use Hellofret\BackEndBundle\Entity\FretConditionne;
use Hellofret\BackEndBundle\Entity\FretPalettes;
use Hellofret\BackEndBundle\Entity\FretVehicule;
use Hellofret\BackEndBundle\Entity\FretDemenagement;
use Hellofret\BackEndBundle\Entity\FretEquipement;
use Hellofret\BackEndBundle\Entity\FretMessagerie;
use Hellofret\BackEndBundle\Entity\FretPharmacie;


use Hellofret\BackEndBundle\Form\FretAlimentationType;
use Hellofret\BackEndBundle\Form\FretAnimauxType;
use Hellofret\BackEndBundle\Form\FretBoisSableType;
use Hellofret\BackEndBundle\Form\FretCerealEpiceType;
use Hellofret\BackEndBundle\Form\FretConteneurType;
use Hellofret\BackEndBundle\Form\FretDechetType;
use Hellofret\BackEndBundle\Form\FretHydrocarbureType;
use Hellofret\BackEndBundle\Form\FretConditionneType;
use Hellofret\BackEndBundle\Form\FretPalettesType;
use Hellofret\BackEndBundle\Form\FretVehiculeType;
use Hellofret\BackEndBundle\Form\FretDemenagementType;
use Hellofret\BackEndBundle\Form\FretEquipementType;
use Hellofret\BackEndBundle\Form\FretMessagerieType;
use Hellofret\BackEndBundle\Form\FretPharmacieType;

class ChargeurController extends Controller
{
    public function indexAction(Request $request)
    {
        
		$profil = $this->getUser()->getProfile() ;
		
		$form = $this->get('form.factory')->create(new ModificationProfileType, $profil);
		$form->remove('metier');
		
		if ($form->handleRequest($request)->isValid()) {
			
			// c'est elle qui déplace l'image là où on veut les stocker
      		$profil->getLogo()->upload();
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($profil);
      		$em->flush();


      		$request->getSession()->getFlashBag()->add('notice', 'Informations bien enregistrée.');
			return $this->render('HellofretBackEndBundle:Chargeur:index.html.twig', array(
						'form' => $form->createView(), 'profil' => $profil,
			));
			
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:index.html.twig', array(
		  			'form' => $form->createView(), 'profil' => $profil,
		));
    }
	public function FretEnCoursAction(Request $request)
    {
        
		$profile = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$listFretEnCours = $repository->fretEnCours($profile) ;	
		
						
		//var_dump( $listFretEnCours[0]->getId() );die();
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:fretEnCours.html.twig', array("frets" => $listFretEnCours));
    }
	public function MesFretsAction(Request $request)
    {
        
		$profile = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$listFretEnCours = $repository->AllFret($profile) ;	
		
						
		//var_dump( $listFretEnCours[0]->getId() );die();
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:mesFrets.html.twig', array("frets" => $listFretEnCours));
    }
	
	public function FretsSauvgarderAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:FretArchive');
		
		$listFretsSauvgarder = $repository->Allarchive($user);
		
		
		$ithems = array();
		
		if($listFretsSauvgarder ){
			foreach( $listFretsSauvgarder as $fret ){
				$ithems[] = $fret->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$fretsSauvgarder = $repository->FretEnregistree($ithems) ;	
		
						
		//var_dump( $listFretEnCours[0]->getId() );die();
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:fretSauvgarder.html.twig', array("frets" => $fretsSauvgarder));
    }
	
	public function FretViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Get name of Fret child
		$entityFret = get_class($fret) ;
		$entityName = str_replace("Hellofret\BackEndBundle\Entity\\","",$entityFret);
		/*$contentFret = $repository->getContentFret($id);*/
		
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		
		$devis =  $repository->findBy(array('annonce' => $fret));
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Message');
		
		$messages =  $repository->findBy(array('annonce' => $fret));
		
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:ViewFret:view'.$entityName.'.html.twig', array("fret" => $fret, "devis" => $devis, "messages" => $messages ));
    }

	public function editAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Get name of Fret child
		$entityFret = get_class($fret) ;
		$entityForm = str_replace("Entity","Form",$entityFret)."Type";
		
		//instancier les objets Entity + Form de Fret en question
		$entityForm = new $entityForm ;
		
		
		$form = $this->get('form.factory')->create($entityForm, $fret);
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Chargeur:editFret.html.twig', array("fret" => $fret, "form" => $form->createView() ));
    }
	
	
	public function refuserDevisAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		
		$devis = $repository->getDevis($id);
		
		if( !$devis ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Refuser le devis
		$devis->setApprobation(false);
		
		$fret = $devis->getAnnonce();
				
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("Le chargeur ".$fret->getProfil()->getProfile()->getNomCommercial()." a refusé votre devis sur son annnonce :".$fret->getTitre());
		$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("devis_refuser");
		$notfication->setProfil($devis->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($fret->getId());
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($devis);
		$em->persist($notfication);
		
		
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Le devis est bien refusé');
		return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
		
    }
	
	public function accepteDevisAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Devis');
		
		$devis = $repository->getDevis($id);
		
		if( !$devis ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Accepter le devis
		$devis->setApprobation(true);
		
		//Fermer l'annonce
		$fret = $devis->getAnnonce();
		$fret->setApprobation(true);
		$fret->setDevisAccepte($devis);
				
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("Le chargeur ".$fret->getProfil()->getProfile()->getNomCommercial()." a accepté votre devis sur son annnonce :".$fret->getTitre());
		$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("devis_accepte");
		$notfication->setProfil($devis->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($fret->getId());
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($devis);
		$em->persist($notfication);
		
		
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Le devis est bien accepté');
		return $this->redirectToRoute('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
		
    }
	
	public function TrajetsDisponibleAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$listTrajetsEnCours = $repository->trajetsDisponible() ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:trajetsDisponible.html.twig', array("trajets" => $listTrajetsEnCours));
    }
	
	public function TrajetsCibleAction(Request $request)
    {
        
		$user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
		
		$demandes = $repository->findBy(array('profil' => $user));
		
		$trajesin = array();
		if( $demandes ){
			foreach( $demandes as $demande ){
				$trajesin[] = $demande->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$listTrajetsEnCours = $repository->TrajetsCible($trajesin) ;	
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:trajetsEnCours.html.twig', array("trajets" => $listTrajetsEnCours));
    }
	
	public function TrajetViewAction($id, Request $request)
    {
		$usr = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		if( !$trajet ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:MessageTrajet');
		
		$messages =  $repository->findBy(array('annonce' => $trajet));
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
					
		$demande = $repository->findBy(array('profil' => $usr, "annonce" => $trajet ));
		
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Trajet:viewTrajet.html.twig', array("trajet" => $trajet, "messages" => $messages, "demandes"=> $demande));
    }
	
	public function TrajetsSauvgarderAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:TrajetArchive');
		
		$listFretsSauvgarder = $repository->Allarchive($user);
		
		
		$ithems = array();
		
		if($listFretsSauvgarder ){
			foreach( $listFretsSauvgarder as $fret ){
				$ithems[] = $fret->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$trajetsSauvgarder = $repository->TrajetsEnregistree($ithems) ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:trajetSauvgarder.html.twig', array("trajets" => $trajetsSauvgarder));
    }
	
	public function DemandeAddAction($id, Request $request)
    {
        $usr = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
					
		$demande = $repository->findOneBy(array('profil' => $usr, "annonce" => $trajet ));
		
		if( (!$trajet) or ($demande) ){
			throw new NotFoundHttpException('Vous pourrez pas faire cette action !');
		}
		
		
		$demande  = new Demande;
		$form = $this->get('form.factory')->create(new DemandeType, $demande);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			//set attribut of Devis
			
			$demande->setProfil($usr);
			$demande->setAnnonce($trajet);
			
			
			//set attribut of Notification
			$notfication  = new Notification;
			$notfication->setTitre("Vous avez reçu une nouvelle demande sur ton annonce :".$trajet->getTitre());
			$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
			$notfication->setUrl($url);
			$notfication->setType("new_demande");
			$notfication->setProfil($trajet->getProfil());
			$notfication->setPrioritaire($this->getUser());
			$notfication->setAnnonce($trajet->getId());
			
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($demande);
			$em->persist($notfication);
			
			
      		$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre demande est bien envoyé.');
			return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Demande:add.html.twig', array("form" => $form->createView(), "trajet" => $trajet ));
    }
	
	public function DemandeEditAction($id, Request $request)
    {
        
		$user = $this->getUser() ;
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
		
		$demande = $repository->findOneBy(array('id' => $id, 'profil' => $user));
		
		if( !$demande ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$trajet = $demande->getAnnonce();
		$form = $this->get('form.factory')->create(new DemandeType, $demande);
		
		if ( $form->handleRequest($request)->isValid() ) {
			
			//set attribut of Notification
			$notfication  = new Notification;
			$notfication->setTitre("Le chargeur ".$demande->getProfil()->getProfile()->getNomCommercial()." a modifié sa propre demande de ton annonce :".$trajet->getTitre());
			$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
			$notfication->setUrl($url);
			$notfication->setType("edit_demande");
			$notfication->setProfil($trajet->getProfil());
			$notfication->setPrioritaire($this->getUser());
			$notfication->setAnnonce($trajet->getId());
			
			
			$em = $this->getDoctrine()->getManager();
      		$em->persist($demande);
			$em->persist($notfication);
			
			
      		$em->flush();
			
			$request->getSession()->getFlashBag()->add('notice', 'Votre demande est bien modifié.');
			return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
		}
		
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Demande:edit.html.twig', array("form" => $form->createView(), "trajet" => $trajet, "demande" => $demande ));
    }
	
	public function refuserDevisDemandeAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:DevisDemande');
		
		$devis = $repository->getDevis($id);
		
		if( !$devis ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Refuser le devis
		$devis->setApprobation(false);
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
					
		$demande = $repository->findOneBy(array('devisDemande' => $devis->getId()));
					
		$trajet = $demande->getAnnonce();
		
				
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("Le chargeur ".$demande->getProfil()->getProfile()->getNomCommercial()." a refusé votre devis sur ton annnonce :".$trajet->getTitre());
		$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("devis_refuser");
		$notfication->setProfil($devis->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($trajet->getId());
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($devis);
		$em->persist($notfication);
		
		
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Le devis est bien refusé');
		return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
		
    }
	
	public function accepteDevisDemandeAction($id, Request $request)
    {
        $repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:DevisDemande');
		
		$devis = $repository->getDevis($id);
		
		if( !$devis ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Accepter le devis
		$devis->setApprobation(true);
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Demande');
					
		$demande = $repository->findOneBy(array('devisDemande' => $devis->getId()));
		$trajet = $demande->getAnnonce();
		
		//Ajouter les devis accéptée
		$alldevis = $trajet->getAllDevis();
		if( !$alldevis ){
			$alldevis = $devis->getId();
		}else{
			$alldevis = $alldevis.",".$devis->getId();
		}
		
		$trajet->setAllDevis($alldevis);
				
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("Le chargeur ".$demande->getProfil()->getProfile()->getNomCommercial()." a accepté votre devis sur ton annnonce :".$trajet->getTitre());
		$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $trajet->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("devis_accepte");
		$notfication->setProfil($devis->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($trajet->getId());
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($devis);
		$em->persist($trajet);
		$em->persist($notfication);
		
		
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', 'Le devis est bien accepté');
		return $this->redirectToRoute('hellofret_chargeur_trajet_view', array('id' => $trajet->getId()) );
		
    }
	public function VoituresDisponibleAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
		$voitures = $repository->findBy(array("disponible" => true ));
			
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:VoituresDisponible.html.twig', array("voitures" => $voitures));
    }
	public function VoitureViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Location:viewVoiture.html.twig', array("voiture" => $voiture));
    }
	
	public function VoituresSauvgarderAction(Request $request)
    {
        
		$user = $this->getUser();
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureArchive');
		
		$voituresSauvgarder = $repository->Allarchive($user);
		
		
		$ithems = array();
		
		if($voituresSauvgarder ){
			foreach( $voituresSauvgarder as $voiture ){
				$ithems[] = $voiture->getAnnonce()->getId();
			}
		}
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
		$voituresSauvgarder = $repository->VoituresEnregistree($ithems) ;	
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:voituresSauvgarder.html.twig', array("voitures" => $voituresSauvgarder));
    }
	
	public function deleteAction($id)
    {
        // Page Template
		return $this->render('HellofretBackEndBundle:Chargeur:delete.html.twig');
    }
}
