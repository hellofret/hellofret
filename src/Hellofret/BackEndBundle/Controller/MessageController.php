<?php

namespace Hellofret\BackEndBundle\Controller;

use Hellofret\BackEndBundle\Entity\Fret;
use Hellofret\BackEndBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Hellofret\BackEndBundle\Entity\Devis;
use Hellofret\BackEndBundle\Entity\Notification;


use Hellofret\BackEndBundle\Entity\FretAlimentation;
use Hellofret\BackEndBundle\Entity\FretAnimaux;
use Hellofret\BackEndBundle\Entity\FretBoisSable;
use Hellofret\BackEndBundle\Entity\FretCerealEpice;
use Hellofret\BackEndBundle\Entity\FretConteneur;
use Hellofret\BackEndBundle\Entity\FretDechet;
use Hellofret\BackEndBundle\Entity\FretHydrocarbure;
use Hellofret\BackEndBundle\Entity\FretConditionne;
use Hellofret\BackEndBundle\Entity\FretPalettes;
use Hellofret\BackEndBundle\Entity\FretVehicule;
use Hellofret\BackEndBundle\Entity\FretDemenagement;
use Hellofret\BackEndBundle\Entity\FretEquipement;
use Hellofret\BackEndBundle\Entity\FretMessagerie;
use Hellofret\BackEndBundle\Entity\FretPharmacie;


class MessageController extends Controller
{
    
	public function addAction(Request $request)
    {
        
		if($request->isXmlHttpRequest()) 
        {
            $fret = $request->request->get('fret') ;
			$content = $request->request->get('content') ;
			$user = $request->request->get('profil') ;
			
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
			$fret = $repository->findOneBy(array('id' => $fret));
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:User');
					
			$user = $repository->findOneBy(array('id' => $user));
			
			
			$message = new Message;
			$message->setContent($content);
			$message->setAnnonce($fret);
			$message->setProfil($user);
			
			$em = $this->getDoctrine()->getManager();
			$em->persist($message);
			
			$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Message');
		
			$messages =  $repository->getMessagesDistinct( $fret, $user->getId() );
			
			
			$ithems = array();
			$ids = array();
			
			foreach( $messages as $msg ){
				//set attribut of Notification
				$notfication  = new Notification;
				$notfication->setTitre("Un nouveau message a été publié dans l'annonce : ".$fret->getTitre());
				if( $msg->getProfil()->hasRole("ROLE_CHARGEUR") ){
					$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
				}else{
					$url = $this->generateUrl('hellofret_transporteur_fret_view', array('id' => $fret->getId()) );
				}
				$notfication->setUrl($url);
				$notfication->setType("new_message");
				$notfication->setProfil($msg->getProfil());
				$notfication->setPrioritaire($this->getUser());
				$notfication->setAnnonce($fret->getId());
				
				$ithems[] = $notfication;
				$ids[] = $msg->getProfil()->getId();
			}
			if( !in_array($fret->getProfil()->getId(),$ids) && $user->getId() != $fret->getProfil()->getId() ){
				$notfication  = new Notification;
				$notfication->setTitre("Un nouveau message a été publié dans l'annonce : ".$fret->getTitre());
				if( $fret->getProfil()->hasRole("ROLE_CHARGEUR") ){
					$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $fret->getId()) );
				}else{
					$url = $this->generateUrl('hellofret_transporteur_fret_view', array('id' => $fret->getId()) );
				}
				$notfication->setUrl($url);
				$notfication->setType("new_message");
				$notfication->setProfil($fret->getProfil());
				$notfication->setPrioritaire($this->getUser());
				$notfication->setAnnonce($fret->getId());
				$em->persist($notfication);
			}
			
      		foreach( $ithems as $ithem ){
				$em->persist($ithem);
			}
			
			
      		$em->flush();
			
			
			$output = array();
			$output[0]["id"] = $message->getId() ;
			
			$output[0]["date"] = date_format($message->getDate(),'d/m/Y H:i:s');
			
			
			$response = new Response();
			$output = json_encode( $output );
			$response->headers->set('Content-Type', 'application/json');
			$response->setContent($output);
			return $response;
        }
		
		return new Response("Erreur, le message n'a pas été envoyé !");
		
    }
	
	
}
