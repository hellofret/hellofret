<?php

namespace Hellofret\BackEndBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Hellofret\BackEndBundle\Entity\Notification;
use Hellofret\BackEndBundle\Entity\Demande;
use Hellofret\BackEndBundle\Form\DemandeType;

use Hellofret\BackEndBundle\Entity\Trajet;
use Hellofret\UserBundle\Entity\Profile;
use Hellofret\UserBundle\Form\ModificationProfileType;

use Hellofret\BackEndBundle\Entity\FretAlimentation;
use Hellofret\BackEndBundle\Entity\FretAnimaux;
use Hellofret\BackEndBundle\Entity\FretBoisSable;
use Hellofret\BackEndBundle\Entity\FretCerealEpice;
use Hellofret\BackEndBundle\Entity\FretConteneur;
use Hellofret\BackEndBundle\Entity\FretDechet;
use Hellofret\BackEndBundle\Entity\FretHydrocarbure;
use Hellofret\BackEndBundle\Entity\FretConditionne;
use Hellofret\BackEndBundle\Entity\FretPalettes;
use Hellofret\BackEndBundle\Entity\FretVehicule;
use Hellofret\BackEndBundle\Entity\FretDemenagement;
use Hellofret\BackEndBundle\Entity\FretEquipement;
use Hellofret\BackEndBundle\Entity\FretMessagerie;
use Hellofret\BackEndBundle\Entity\FretPharmacie;


use Hellofret\BackEndBundle\Form\FretAlimentationType;
use Hellofret\BackEndBundle\Form\FretAnimauxType;
use Hellofret\BackEndBundle\Form\FretBoisSableType;
use Hellofret\BackEndBundle\Form\FretCerealEpiceType;
use Hellofret\BackEndBundle\Form\FretConteneurType;
use Hellofret\BackEndBundle\Form\FretDechetType;
use Hellofret\BackEndBundle\Form\FretHydrocarbureType;
use Hellofret\BackEndBundle\Form\FretConditionneType;
use Hellofret\BackEndBundle\Form\FretPalettesType;
use Hellofret\BackEndBundle\Form\FretVehiculeType;
use Hellofret\BackEndBundle\Form\FretDemenagementType;
use Hellofret\BackEndBundle\Form\FretEquipementType;
use Hellofret\BackEndBundle\Form\FretMessagerieType;
use Hellofret\BackEndBundle\Form\FretPharmacieType;

class AdminController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('HellofretBackEndBundle:Admin:index.html.twig');
    }
	
	
	public function FretsProposeeAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$annonces =  $repository->findBy(array('disponible' => false));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:fretsProposee.html.twig', array("annonces" => $annonces));
    }
	
	public function TrajetsProposeeAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$annonces =  $repository->findBy(array('disponible' => false));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:trajetsProposee.html.twig', array("annonces" => $annonces));
    }
	
	public function VehiculesProposeeAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
		$annonces =  $repository->findBy(array('disponible' => false));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:vehiculesProposee.html.twig', array("annonces" => $annonces));
    }
	
	public function FretApprouverAction($id, Request $request)
    {
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$annonce = $repository->findOneBy(array('id' => $id));
		
		if( !$annonce ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$annonce->setDisponible( true );
		
		
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("L'équipe éditoriale de hellofret.com a accepté votre annonce : ".$annonce->getTitre());
		$url = $this->generateUrl('hellofret_chargeur_fret_view', array('id' => $annonce->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("annonce_approuver");
		$notfication->setProfil($annonce->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($annonce->getId());
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($annonce);
		$em->persist($notfication);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', "L'annonce Numéro : ".$annonce->getId()." est bien approuvé !");
		return $this->redirectToRoute('hellofret_admin_frets_attend' );
    }
	
	public function TrajetApprouverAction($id, Request $request)
    {
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$annonce = $repository->findOneBy(array('id' => $id));
		
		if( !$annonce ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$annonce->setDisponible( true );
		
		
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("L'équipe éditoriale de hellofret.com a accepté votre annonce : ".$annonce->getTitre());
		$url = $this->generateUrl('hellofret_transporteur_trajet_view', array('id' => $annonce->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("annonce_approuver");
		$notfication->setProfil($annonce->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($annonce->getId());
		
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($notfication);
		$em->persist($annonce);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', "L'annonce Numéro : ".$annonce->getId()." est bien approuvé !");
		return $this->redirectToRoute('hellofret_admin_trajets_attend' );
    }
	
	public function VehiculesApprouverAction($id, Request $request)
    {
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$annonce = $repository->findOneBy(array('id' => $id));
		
		if( !$annonce ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		$annonce->setDisponible( true );
		
		//set attribut of Notification
		$notfication  = new Notification;
		$notfication->setTitre("L'équipe éditoriale de hellofret.com a accepté votre annonce : ".$annonce->getTitre());
		$url = $this->generateUrl('hellofret_transporteur_voiture_view', array('id' => $annonce->getId()) );
		$notfication->setUrl($url);
		$notfication->setType("annonce_approuver");
		$notfication->setProfil($annonce->getProfil());
		$notfication->setPrioritaire($this->getUser());
		$notfication->setAnnonce($annonce->getId());
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($annonce);
		$em->persist($notfication);
		$em->flush();
		
		$request->getSession()->getFlashBag()->add('notice', "L'annonce Numéro : ".$annonce->getId()." est bien approuvé !");
		return $this->redirectToRoute('hellofret_admin_vehicules_attend' );
    }
	
	
	public function FretViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
					
		$fret = $repository->findOneBy(array('id' => $id));
		
		if( !$fret ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		//Get name of Fret child
		$entityFret = get_class($fret) ;
		$entityName = str_replace("Hellofret\BackEndBundle\Entity\\","",$entityFret);
		/*$contentFret = $repository->getContentFret($id);*/
		
		
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:ViewFret:view'.$entityName.'.html.twig', array("fret" => $fret ));
    }
	
	
	public function TrajetViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
					
		$trajet = $repository->findOneBy(array('id' => $id));
		
		if( !$trajet ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Trajet:viewTrajet.html.twig', array("trajet" => $trajet));
    }
	
	public function VehiculeViewAction($id, Request $request)
    {
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
					
		$voiture = $repository->findOneBy(array('id' => $id));
		
		if( !$voiture ){
			throw new NotFoundHttpException('Page introuvable !');
		}
		
		// Page Template
		
		return $this->render('HellofretBackEndBundle:Location:viewVoiture.html.twig', array("voiture" => $voiture));
    }
	
	public function AlltransporteursAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:Profile');
		
		$Profiles =  $repository->findBy(array('typeUser' => 'transporteur'));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:transporteurs.html.twig', array("Profiles" => $Profiles));
    }
	public function AllchargeursAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:Profile');
		
		$Profiles =  $repository->findBy(array('typeUser' => 'chargeur'));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:chargeurs.html.twig', array("Profiles" => $Profiles));
    }
	
	public function FretsDisponibleAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Fret');
		
		$annonces =  $repository->findBy(array('disponible' => true));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:fretsDisponible.html.twig', array("annonces" => $annonces));
    }
	
	public function TrajetsDisponibleAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:Trajet');
		
		$annonces =  $repository->findBy(array('disponible' => true));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:trajetsDisponible.html.twig', array("annonces" => $annonces));
    }
	
	public function VehiculesDisponibleAction(Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretBackEndBundle:VoitureLocation');
		
		$annonces =  $repository->findBy(array('disponible' => true));
		
		//var_dump($annonces);die();
		return $this->render('HellofretBackEndBundle:Admin:vehiculesDisponible.html.twig', array("annonces" => $annonces));
    }
	
	public function transporteurViewAction($id, Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:Profile');
		
		$profil =  $repository->findOneBy(array('id' => $id));
		
		$form = $this->get('form.factory')->create(new ModificationProfileType, $profil);
		$form->remove('metier');
		$form->remove('logo');
		$form->remove('Enregistrer');
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:User');
		
		$user =  $repository->findOneBy(array('profile' => $profil));
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Admin:transporteurView.html.twig', array(
		  			'form' => $form->createView(), 'profil' => $profil,'user' => $user
		));
    }
	
	public function chargeurViewAction($id, Request $request)
    {
        
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:Profile');
		
		$profil =  $repository->findOneBy(array('id' => $id));
		
		$form = $this->get('form.factory')->create(new ModificationProfileType, $profil);
		$form->remove('metier');
		$form->remove('logo');
		$form->remove('Enregistrer');
		
		$repository = $this
					->getDoctrine()
					->getManager()
					->getRepository('HellofretUserBundle:User');
		
		$user =  $repository->findOneBy(array('profile' => $profil));
		
		// Page Template
		return $this->render('HellofretBackEndBundle:Admin:chargeurView.html.twig', array(
		  			'form' => $form->createView(), 'profil' => $profil,'user' => $user
		));
    }
	
}
