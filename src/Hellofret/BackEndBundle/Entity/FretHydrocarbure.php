<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretHydrocarbure
 *
 * @ORM\Table(name="fret_hydrocarbure")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretHydrocarbureRepository")
 */
class FretHydrocarbure extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poidsCertain", type="boolean", nullable=true)
     */
    private $poidsCertain;


    
    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretHydrocarbure
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return FretHydrocarbure
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return FretHydrocarbure
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }
}

