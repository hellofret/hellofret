<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Demande
 *
 * @ORM\Table(name="hel_demande")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\DemandeRepository")
 */
class Demande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\CategorieMode")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="villeDepart", type="string", length=255)
     */
    private $villeDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="villeArrivee", type="string", length=255)
     */
    private $villeArrivee;

    /**
     * @var string
     *
     * @ORM\Column(name="informationComplementaires", type="text")
     */
    private $informationComplementaires;
	
	/**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\UserBundle\Entity\User")
     */
    private $profil;
	
	/**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\Trajet")
     */
    private $annonce;
	
	/**
     * @var string
     *
     * @ORM\Column(name="date_pub", type="datetime", nullable=true)
     */
    private $datePub;
	
	/**
     * @var bool
     *
     * @ORM\Column(name="approbation", type="boolean", nullable=true)
     */
    private $approbation;
	
	/**
     *
     * @ORM\OneToOne(targetEntity="Hellofret\BackEndBundle\Entity\DevisDemande", cascade={"remove"})
	 * @ORM\JoinColumn(nullable=true)
     */
    private $devisDemande;


	public function __construct()
	{
		$this->datePub = new \Datetime();
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Demande
     */
    public function setCategorie(CategorieMode $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set villeDepart
     *
     * @param string $villeDepart
     *
     * @return Demande
     */
    public function setVilleDepart($villeDepart)
    {
        $this->villeDepart = $villeDepart;

        return $this;
    }

    /**
     * Get villeDepart
     *
     * @return string
     */
    public function getVilleDepart()
    {
        return $this->villeDepart;
    }

    /**
     * Set villeArrivee
     *
     * @param string $villeArrivee
     *
     * @return Demande
     */
    public function setVilleArrivee($villeArrivee)
    {
        $this->villeArrivee = $villeArrivee;

        return $this;
    }

    /**
     * Get villeArrivee
     *
     * @return string
     */
    public function getVilleArrivee()
    {
        return $this->villeArrivee;
    }

    /**
     * Set informationComplementaires
     *
     * @param string $informationComplementaires
     *
     * @return Demande
     */
    public function setInformationComplementaires($informationComplementaires)
    {
        $this->informationComplementaires = $informationComplementaires;

        return $this;
    }

    /**
     * Get informationComplementaires
     *
     * @return string
     */
    public function getInformationComplementaires()
    {
        return $this->informationComplementaires;
    }

    /**
     * Set datePub
     *
     * @param \DateTime $datePub
     *
     * @return Demande
     */
    public function setDatePub($datePub)
    {
        $this->datePub = $datePub;

        return $this;
    }

    /**
     * Get datePub
     *
     * @return \DateTime
     */
    public function getDatePub()
    {
        return $this->datePub;
    }

    /**
     * Set approbation
     *
     * @param boolean $approbation
     *
     * @return Demande
     */
    public function setApprobation($approbation)
    {
        $this->approbation = $approbation;

        return $this;
    }

    /**
     * Get approbation
     *
     * @return boolean
     */
    public function getApprobation()
    {
        return $this->approbation;
    }

    /**
     * Set profil
     *
     * @param \Hellofret\UserBundle\Entity\User $profil
     *
     * @return Demande
     */
    public function setProfil(\Hellofret\UserBundle\Entity\User $profil = null)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return \Hellofret\UserBundle\Entity\User
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set annonce
     *
     * @param \Hellofret\BackEndBundle\Entity\Trajet $annonce
     *
     * @return Demande
     */
    public function setAnnonce(\Hellofret\BackEndBundle\Entity\Trajet $annonce = null)
    {
        $this->annonce = $annonce;

        return $this;
    }

    /**
     * Get annonce
     *
     * @return \Hellofret\BackEndBundle\Entity\Trajet
     */
    public function getAnnonce()
    {
        return $this->annonce;
    }

    /**
     * Set devisDemande
     *
     * @param \Hellofret\BackEndBundle\Entity\DevisDemande $devisDemande
     *
     * @return Demande
     */
    public function setDevisDemande(\Hellofret\BackEndBundle\Entity\DevisDemande $devisDemande = null)
    {
        $this->devisDemande = $devisDemande;

        return $this;
    }

    /**
     * Get devisDemande
     *
     * @return \Hellofret\BackEndBundle\Entity\DevisDemande
     */
    public function getDevisDemande()
    {
        return $this->devisDemande;
    }
}
