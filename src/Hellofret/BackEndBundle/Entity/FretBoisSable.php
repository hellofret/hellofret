<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretBoisSable
 *
 * @ORM\Table(name="fret_bois_sable")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretBoisSableRepository")
 */
class FretBoisSable extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poidsCertain", type="boolean", nullable=true)
     */
    private $poidsCertain;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var bool
     *
     * @ORM\Column(name="quantiteCertain", type="boolean", nullable=true)
     */
    private $quantiteCertain;

    /**
     * @var int
     *
     * @ORM\Column(name="volume", type="integer", nullable=true)
     */
    private $volume;

    /**
     * @var bool
     *
     * @ORM\Column(name="volumeCertain", type="boolean", nullable=true)
     */
    private $volumeCertain;


   

    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretBoisSable
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return FretBoisSable
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return FretBoisSable
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return FretBoisSable
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set quantiteCertain
     *
     * @param boolean $quantiteCertain
     *
     * @return FretBoisSable
     */
    public function setQuantiteCertain($quantiteCertain)
    {
        $this->quantiteCertain = $quantiteCertain;

        return $this;
    }

    /**
     * Get quantiteCertain
     *
     * @return bool
     */
    public function getQuantiteCertain()
    {
        return $this->quantiteCertain;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     *
     * @return FretBoisSable
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return int
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set volumeCertain
     *
     * @param boolean $volumeCertain
     *
     * @return FretBoisSable
     */
    public function setVolumeCertain($volumeCertain)
    {
        $this->volumeCertain = $volumeCertain;

        return $this;
    }

    /**
     * Get volumeCertain
     *
     * @return bool
     */
    public function getVolumeCertain()
    {
        return $this->volumeCertain;
    }
}

