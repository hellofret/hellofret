<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trajet
 *
 * @ORM\Table(name="hel_trajet")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\TrajetRepository")
 */
class Trajet extends Annonce
{
    
	
	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\Mode")
     */
    private $mode;
	
	/**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\CategorieMode")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="villeDepart", type="string", length=255, nullable=true)
     */
    private $villeDepart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDepart", type="datetime", nullable=true)
     */
    private $dateDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="villeArrive", type="string", length=255, nullable=true)
     */
    private $villeArrive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateArrive", type="datetime", nullable=true)
     */
    private $dateArrive;

    /**
     * @var string
     *
     * @ORM\Column(name="escales", type="text", nullable=true)
     */
    private $escales;

    

    /**
     * @var string
     *
     * @ORM\Column(name="typePeriode", type="string", length=255, nullable=true)
     */
    private $typePeriode;

    /**
     * @var int
     *
     * @ORM\Column(name="nombrePeriode", type="integer", nullable=true)
     */
    private $nombrePeriode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExpiration", type="datetime", nullable=true)
     */
    private $dateExpiration;
	
	/**
     * @var string
     *
     * @ORM\Column(name="informationsComplementaires", type="text", nullable=true)
     */
    private $informationsComplementaires;
	
	/**
     * @var string
     *
     * @ORM\Column(name="all_devis", type="string", length=255, nullable=true)
     */
    private $allDevis;
	
	/**
     * @var bool
     *
     * @ORM\Column(name="departIsFlexible", type="boolean", nullable=true)
     */
    private $departIsFlexible;
	
	/**
     * @var string
     *
     * @ORM\Column(name="dateDepartFlexible", type="string", nullable=true)
     */
    private $dateDepartFlexible;
	
	/**
     * @var bool
     *
     * @ORM\Column(name="arriveIsFlexible", type="boolean", nullable=true)
     */
    private $arriveIsFlexible;

	/**
     * @var string
     *
     * @ORM\Column(name="dateArriveeFlexible", type="string", nullable=true)
     */
    private $dateArriveeFlexible;



    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return Trajet
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set villeDepart
     *
     * @param string $villeDepart
     *
     * @return Trajet
     */
    public function setVilleDepart($villeDepart)
    {
        $this->villeDepart = $villeDepart;

        return $this;
    }

    /**
     * Get villeDepart
     *
     * @return string
     */
    public function getVilleDepart()
    {
        return $this->villeDepart;
    }

    /**
     * Set dateDepart
     *
     * @param \DateTime $dateDepart
     *
     * @return Trajet
     */
    public function setDateDepart($dateDepart)
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    /**
     * Get dateDepart
     *
     * @return \DateTime
     */
    public function getDateDepart()
    {
        return $this->dateDepart;
    }

    /**
     * Set villeArrive
     *
     * @param string $villeArrive
     *
     * @return Trajet
     */
    public function setVilleArrive($villeArrive)
    {
        $this->villeArrive = $villeArrive;

        return $this;
    }

    /**
     * Get villeArrive
     *
     * @return string
     */
    public function getVilleArrive()
    {
        return $this->villeArrive;
    }

    /**
     * Set dateArrive
     *
     * @param \DateTime $dateArrive
     *
     * @return Trajet
     */
    public function setDateArrive($dateArrive)
    {
        $this->dateArrive = $dateArrive;

        return $this;
    }

    /**
     * Get dateArrive
     *
     * @return \DateTime
     */
    public function getDateArrive()
    {
        return $this->dateArrive;
    }

    /**
     * Set escales
     *
     * @param string $escales
     *
     * @return Trajet
     */
    public function setEscales($escales)
    {
        $this->escales = $escales;

        return $this;
    }

    /**
     * Get escales
     *
     * @return string
     */
    public function getEscales()
    {
        return $this->escales;
    }

    /**
     * Set departItineraire
     *
     * @param string $departItineraire
     *
     * @return Trajet
     */
    public function setDepartItineraire($departItineraire)
    {
        $this->departItineraire = $departItineraire;

        return $this;
    }

    /**
     * Get departItineraire
     *
     * @return string
     */
    public function getDepartItineraire()
    {
        return $this->departItineraire;
    }

    /**
     * Set arriveItineraire
     *
     * @param string $arriveItineraire
     *
     * @return Trajet
     */
    public function setArriveItineraire($arriveItineraire)
    {
        $this->arriveItineraire = $arriveItineraire;

        return $this;
    }

    /**
     * Get arriveItineraire
     *
     * @return string
     */
    public function getArriveItineraire()
    {
        return $this->arriveItineraire;
    }

    /**
     * Set typePeriode
     *
     * @param string $typePeriode
     *
     * @return Trajet
     */
    public function setTypePeriode($typePeriode)
    {
        $this->typePeriode = $typePeriode;

        return $this;
    }

    /**
     * Get typePeriode
     *
     * @return string
     */
    public function getTypePeriode()
    {
        return $this->typePeriode;
    }

    /**
     * Set nombrePeriode
     *
     * @param integer $nombrePeriode
     *
     * @return Trajet
     */
    public function setNombrePeriode($nombrePeriode)
    {
        $this->nombrePeriode = $nombrePeriode;

        return $this;
    }

    /**
     * Get nombrePeriode
     *
     * @return int
     */
    public function getNombrePeriode()
    {
        return $this->nombrePeriode;
    }

    /**
     * Set dateExpiration
     *
     * @param \DateTime $dateExpiration
     *
     * @return Trajet
     */
    public function setDateExpiration($dateExpiration)
    {
        $this->dateExpiration = $dateExpiration;

        return $this;
    }

    /**
     * Get dateExpiration
     *
     * @return \DateTime
     */
    public function getDateExpiration()
    {
        return $this->dateExpiration;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Trajet
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set categorie
     *
     * @param \Hellofret\BackEndBundle\Entity\CategorieMode $categorie
     *
     * @return Trajet
     */
    public function setCategorie(\Hellofret\BackEndBundle\Entity\CategorieMode $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Hellofret\BackEndBundle\Entity\CategorieMode
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set informationsComplementaires
     *
     * @param string $informationsComplementaires
     *
     * @return Trajet
     */
    public function setInformationsComplementaires($informationsComplementaires)
    {
        $this->informationsComplementaires = $informationsComplementaires;

        return $this;
    }

    /**
     * Get informationsComplementaires
     *
     * @return string
     */
    public function getInformationsComplementaires()
    {
        return $this->informationsComplementaires;
    }

    /**
     * Set allDevis
     *
     * @param string $allDevis
     *
     * @return Trajet
     */
    public function setAllDevis($allDevis)
    {
        $this->allDevis = $allDevis;

        return $this;
    }

    /**
     * Get allDevis
     *
     * @return string
     */
    public function getAllDevis()
    {
        return $this->allDevis;
    }

    /**
     * Set departIsFlexible
     *
     * @param boolean $departIsFlexible
     *
     * @return Trajet
     */
    public function setDepartIsFlexible($departIsFlexible)
    {
        $this->departIsFlexible = $departIsFlexible;

        return $this;
    }

    /**
     * Get departIsFlexible
     *
     * @return boolean
     */
    public function getDepartIsFlexible()
    {
        return $this->departIsFlexible;
    }

    /**
     * Set dateDepartFlexible
     *
     * @param string $dateDepartFlexible
     *
     * @return Trajet
     */
    public function setDateDepartFlexible($dateDepartFlexible)
    {
        $this->dateDepartFlexible = $dateDepartFlexible;

        return $this;
    }

    /**
     * Get dateDepartFlexible
     *
     * @return string
     */
    public function getDateDepartFlexible()
    {
        return $this->dateDepartFlexible;
    }

    /**
     * Set arriveIsFlexible
     *
     * @param boolean $arriveIsFlexible
     *
     * @return Trajet
     */
    public function setArriveIsFlexible($arriveIsFlexible)
    {
        $this->arriveIsFlexible = $arriveIsFlexible;

        return $this;
    }

    /**
     * Get arriveIsFlexible
     *
     * @return boolean
     */
    public function getArriveIsFlexible()
    {
        return $this->arriveIsFlexible;
    }

    /**
     * Set dateArriveeFlexible
     *
     * @param string $dateArriveeFlexible
     *
     * @return Trajet
     */
    public function setDateArriveeFlexible($dateArriveeFlexible)
    {
        $this->dateArriveeFlexible = $dateArriveeFlexible;

        return $this;
    }

    /**
     * Get dateArriveeFlexible
     *
     * @return string
     */
    public function getDateArriveeFlexible()
    {
        return $this->dateArriveeFlexible;
    }
}
