<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretEquipement
 *
 * @ORM\Table(name="fret_equipement")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretEquipementRepository")
 */
class FretEquipement extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;



    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretEquipement
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }
}

