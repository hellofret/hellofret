<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VoitureLocation
 *
 * @ORM\Table(name="voiture_location")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\VoitureLocationRepository")
 */
class VoitureLocation extends Annonce
{
    

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\CategorieMode")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="marque", type="string", length=255)
     */
    private $marque;

    /**
     * @var string
     *
     * @ORM\Column(name="modele", type="string", length=255)
     */
    private $modele;

    /**
     * @var string
     *
     * @ORM\Column(name="informations", type="text")
     */
    private $informations;

    /**
     * @var int
     *
     * @ORM\Column(name="prix", type="integer", length=255)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Hellofret\BackEndBundle\Entity\Image", cascade={"remove", "persist"})
     */
    private $photos;


    

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return VoitureLocation
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set marque
     *
     * @param string $marque
     *
     * @return VoitureLocation
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get marque
     *
     * @return string
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * Set modele
     *
     * @param string $modele
     *
     * @return VoitureLocation
     */
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }

    /**
     * Get modele
     *
     * @return string
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set informations
     *
     * @param string $informations
     *
     * @return VoitureLocation
     */
    public function setInformations($informations)
    {
        $this->informations = $informations;

        return $this;
    }

    /**
     * Get informations
     *
     * @return string
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * Set prix
     *
     * @param string $prix
     *
     * @return VoitureLocation
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return string
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return VoitureLocation
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set photos
     *
     * @param string $photos
     *
     * @return VoitureLocation
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * Get photos
     *
     * @return string
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * Add photo
     *
     * @param \Hellofret\BackEndBundle\Entity\Image $photo
     *
     * @return VoitureLocation
     */
    public function addPhoto(\Hellofret\BackEndBundle\Entity\Image $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \Hellofret\BackEndBundle\Entity\Image $photo
     */
    public function removePhoto(\Hellofret\BackEndBundle\Entity\Image $photo)
    {
        $this->photos->removeElement($photo);
    }
}
