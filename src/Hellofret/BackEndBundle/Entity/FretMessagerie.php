<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretMessagerie
 *
 * @ORM\Table(name="fret_messagerie")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretMessagerieRepository")
 */
class FretMessagerie extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poidsCertain", type="boolean", nullable=true)
     */
    private $poidsCertain;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var bool
     *
     * @ORM\Column(name="quantiteCertain", type="boolean", nullable=true)
     */
    private $quantiteCertain;

    /**
     * @var string
     *
     * @ORM\Column(name="typeMessagerie", type="string", length=255, nullable=true)
     */
    private $typeMessagerie;
	
	/**
     * @var string
     *
     * @ORM\Column(name="fragile", type="string", length=255, nullable=true)
     */
    private $fragile;


    

    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretMessagerie
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return FretMessagerie
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return FretMessagerie
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return FretMessagerie
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set quantiteCertain
     *
     * @param boolean $quantiteCertain
     *
     * @return FretMessagerie
     */
    public function setQuantiteCertain($quantiteCertain)
    {
        $this->quantiteCertain = $quantiteCertain;

        return $this;
    }

    /**
     * Get quantiteCertain
     *
     * @return bool
     */
    public function getQuantiteCertain()
    {
        return $this->quantiteCertain;
    }

    /**
     * Set typeMessagerie
     *
     * @param string $typeMessagerie
     *
     * @return FretMessagerie
     */
    public function setTypeMessagerie($typeMessagerie)
    {
        $this->typeMessagerie = $typeMessagerie;

        return $this;
    }

    /**
     * Get typeMessagerie
     *
     * @return string
     */
    public function getTypeMessagerie()
    {
        return $this->typeMessagerie;
    }

    /**
     * Set fragile
     *
     * @param string $fragile
     *
     * @return FretMessagerie
     */
    public function setFragile($fragile)
    {
        $this->fragile = $fragile;

        return $this;
    }

    /**
     * Get fragile
     *
     * @return string
     */
    public function getFragile()
    {
        return $this->fragile;
    }
}
