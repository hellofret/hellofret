<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretDemenagement
 *
 * @ORM\Table(name="fret_demenagement")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretDemenagementRepository")
 */
class FretDemenagement extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="typeDemenagement", type="string", length=255, nullable=true)
     */
    private $typeDemenagement;

    /**
     * @var string
     *
     * @ORM\Column(name="detailsDemenagement", type="text", nullable=true)
     */
    private $detailsDemenagement;



    /**
     * Set typeDemenagement
     *
     * @param string $typeDemenagement
     *
     * @return FretDemenagement
     */
    public function setTypeDemenagement($typeDemenagement)
    {
        $this->typeDemenagement = $typeDemenagement;

        return $this;
    }

    /**
     * Get typeDemenagement
     *
     * @return string
     */
    public function getTypeDemenagement()
    {
        return $this->typeDemenagement;
    }

    /**
     * Set detailsDemenagement
     *
     * @param string $detailsDemenagement
     *
     * @return FretDemenagement
     */
    public function setDetailsDemenagement($detailsDemenagement)
    {
        $this->detailsDemenagement = $detailsDemenagement;

        return $this;
    }

    /**
     * Get detailsDemenagement
     *
     * @return string
     */
    public function getDetailsDemenagement()
    {
        return $this->detailsDemenagement;
    }
}

