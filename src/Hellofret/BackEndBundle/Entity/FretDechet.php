<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretDechet
 *
 * @ORM\Table(name="fret_dechet")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretDechetRepository")
 */
class FretDechet extends Fret
{

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poidsCertain", type="boolean", nullable=true)
     */
    private $poidsCertain;

    /**
     * @var int
     *
     * @ORM\Column(name="volume", type="integer", nullable=true)
     */
    private $volume;

    /**
     * @var bool
     *
     * @ORM\Column(name="volumeCertain", type="boolean", nullable=true)
     */
    private $volumeCertain;



    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretDechet
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return FretDechet
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return FretDechet
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     *
     * @return FretDechet
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return int
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set volumeCertain
     *
     * @param boolean $volumeCertain
     *
     * @return FretDechet
     */
    public function setVolumeCertain($volumeCertain)
    {
        $this->volumeCertain = $volumeCertain;

        return $this;
    }

    /**
     * Get volumeCertain
     *
     * @return bool
     */
    public function getVolumeCertain()
    {
        return $this->volumeCertain;
    }
}

