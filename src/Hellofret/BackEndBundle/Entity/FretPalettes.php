<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretPalettes
 *
 * @ORM\Table(name="fret_palettes")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretPalettesRepository")
 */
class FretPalettes extends Fret
{
    

    /**
     *
     * @ORM\ManyToMany(targetEntity="Hellofret\BackEndBundle\Entity\Palette", cascade={"remove", "persist"})
     */
    private $palettes;


    /**
     * Set palettes
     *
     * @param string $palettes
     *
     * @return FretPalettes
     */
    public function setPalettes($palettes)
    {
        $this->palettes = $palettes;

        return $this;
    }

    /**
     * Get palettes
     *
     * @return string
     */
    public function getPalettes()
    {
        return $this->palettes;
    }
}
