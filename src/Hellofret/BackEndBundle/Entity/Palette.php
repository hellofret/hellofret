<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Palette
 *
 * @ORM\Table(name="hel_palette")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\PaletteRepository")
 */
class Palette
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(name="volume", type="string", length=255, nullable=true)
     */
    private $volume;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poids_certain", type="boolean", nullable=true)
     */
    private $poidsCertain;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Palette
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set volume
     *
     * @param string $volume
     *
     * @return Palette
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return string
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return Palette
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return Palette
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }
}

