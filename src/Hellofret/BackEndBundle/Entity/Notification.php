<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="hel_notification")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

	/**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
	
    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;
	
	
    /**
     * @var bool
     *
     * @ORM\Column(name="etat", type="boolean", nullable=true)
     */
    private $etat;
	
	/**
     * @var bool
     *
     * @ORM\Column(name="showed", type="boolean", nullable=true)
     */
    private $showed;
	
	/**
     * @var bool
     *
     * @ORM\Column(name="razed", type="boolean", nullable=true)
     */
    private $razed;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\UserBundle\Entity\User")
     */
    private $profil;
	
	/**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\UserBundle\Entity\User")
     */
    private $prioritaire;
	
	/**
     * @var int
     *
     * @ORM\Column(name="id_annonce", type="integer", nullable=true)
     */
    private $annonce;

	
	public function __construct()
	{
		$this->etat = false;
		$this->showed = false;
		$this->razed = false;
	}
	
	
	
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Notification
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }
	
	
	/**
     * Set type
     *
     * @param string $type
     *
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Notification
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set etat
     *
     * @param boolean $etat
     *
     * @return Notification
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return bool
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set profil
     *
     * @param string $profil
     *
     * @return Notification
     */
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return string
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set prioritaire
     *
     * @param \Hellofret\UserBundle\Entity\User $prioritaire
     *
     * @return Notification
     */
    public function setPrioritaire(\Hellofret\UserBundle\Entity\User $prioritaire = null)
    {
        $this->prioritaire = $prioritaire;

        return $this;
    }

    /**
     * Get prioritaire
     *
     * @return \Hellofret\UserBundle\Entity\User
     */
    public function getPrioritaire()
    {
        return $this->prioritaire;
    }

    /**
     * Set annonce
     *
     * @param integer $annonce
     *
     * @return Notification
     */
    public function setAnnonce($annonce)
    {
        $this->annonce = $annonce;

        return $this;
    }

    /**
     * Get annonce
     *
     * @return integer
     */
    public function getAnnonce()
    {
        return $this->annonce;
    }

    /**
     * Set show
     *
     * @param boolean $show
     *
     * @return Notification
     */
    public function setShowed($show)
    {
        $this->show = $show;

        return $this;
    }

    /**
     * Get show
     *
     * @return boolean
     */
    public function getShowed()
    {
        return $this->show;
    }

    /**
     * Set razed
     *
     * @param boolean $razed
     *
     * @return Notification
     */
    public function setRazed($razed)
    {
        $this->razed = $razed;

        return $this;
    }

    /**
     * Get razed
     *
     * @return boolean
     */
    public function getRazed()
    {
        return $this->razed;
    }
}
