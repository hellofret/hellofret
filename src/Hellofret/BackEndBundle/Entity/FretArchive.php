<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretArchive
 *
 * @ORM\Table(name="fret_archive")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretArchiveRepository")
 */
class FretArchive
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\Fret")
     */
    private $annonce;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\UserBundle\Entity\User")
     */
    private $profil;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annonce
     *
     * @param string $annonce
     *
     * @return FretArchive
     */
    public function setAnnonce($annonce)
    {
        $this->annonce = $annonce;

        return $this;
    }

    /**
     * Get annonce
     *
     * @return string
     */
    public function getAnnonce()
    {
        return $this->annonce;
    }

    /**
     * Set profil
     *
     * @param string $profil
     *
     * @return FretArchive
     */
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return string
     */
    public function getProfil()
    {
        return $this->profil;
    }
}

