<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fret
 *
 * @ORM\Table(name="hel_fret")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type_annonce", type="string")
 * @ORM\DiscriminatorMap({"fret" = "Fret", "fretalimentation" = "FretAlimentation", "fretanimaux" = "FretAnimaux", "fretboisSable" = "FretBoisSable", "fretcerealepice" = "FretCerealEpice", "fretconditionne" = "FretConditionne", "fretconteneur" = "FretConteneur", "fretdechet" = "FretDechet", "fretdemenagement" = "FretDemenagement", "fretequipement" = "FretEquipement", "frethydrocarbure" = "FretHydrocarbure", "fretmessagerie" = "FretMessagerie", "fretpalettes" = "FretPalettes", "fretpharmacie" = "FretPharmacie", "fretvehicule" = "FretVehicule"})
 */
class Fret extends Annonce
{
	
   

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\Mode")
     */
    private $mode;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\CategorieMode")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="villeDepart", type="string", length=255, nullable=true)
     */
    private $villeDepart;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateDepart", type="datetime", nullable=true)
     */
    private $dateDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="villeArrivee", type="string", length=255, nullable=true)
     */
    private $villeArrivee;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateArrivee", type="datetime", nullable=true)
     */
    private $dateArrivee;

    /**
     * @var string
     *
     * @ORM\Column(name="rueDepart", type="string", length=255, nullable=true)
     */
    private $rueDepart;

    /**
     * @var string
     *
     * @ORM\Column(name="rueArrivee", type="string", length=255, nullable=true)
     */
    private $rueArrivee;

    /**
     * @var string
     *
     * @ORM\Column(name="informationsComplementaires", type="text", nullable=true)
     */
    private $informationsComplementaires;
	
	
	/**
     * @var bool
     *
     * @ORM\Column(name="departIsFlexible", type="boolean", nullable=true)
     */
    private $departIsFlexible;
	
	/**
     * @var string
     *
     * @ORM\Column(name="dateDepartFlexible", type="string", nullable=true)
     */
    private $dateDepartFlexible;
	
	/**
     * @var bool
     *
     * @ORM\Column(name="arriveIsFlexible", type="boolean", nullable=true)
     */
    private $arriveIsFlexible;

	/**
     * @var string
     *
     * @ORM\Column(name="dateArriveeFlexible", type="string", nullable=true)
     */
    private $dateArriveeFlexible;
    

    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return Annonce
     */
    public function setMode(Mode $mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     *
     * @return Annonce
     */
    public function setCategorie(CategorieMode $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set villeDepart
     *
     * @param string $villeDepart
     *
     * @return Annonce
     */
    public function setVilleDepart($villeDepart)
    {
        $this->villeDepart = $villeDepart;

        return $this;
    }

    /**
     * Get villeDepart
     *
     * @return string
     */
    public function getVilleDepart()
    {
        return $this->villeDepart;
    }

    /**
     * Set dateDepart
     *
     * @param \DateTime $dateDepart
     *
     * @return Annonce
     */
    public function setDateDepart($dateDepart)
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    /**
     * Get dateDepart
     *
     * @return \DateTime
     */
    public function getDateDepart()
    {
        return $this->dateDepart;
    }

    /**
     * Set villeArrivee
     *
     * @param string $villeArrivee
     *
     * @return Annonce
     */
    public function setVilleArrivee($villeArrivee)
    {
        $this->villeArrivee = $villeArrivee;

        return $this;
    }

    /**
     * Get villeArrivee
     *
     * @return string
     */
    public function getVilleArrivee()
    {
        return $this->villeArrivee;
    }

    /**
     * Set dateArrivee
     *
     * @param \DateTime $dateArrivee
     *
     * @return Annonce
     */
    public function setDateArrivee($dateArrivee)
    {
        $this->dateArrivee = $dateArrivee;

        return $this;
    }

    /**
     * Get dateArrivee
     *
     * @return \DateTime
     */
    public function getDateArrivee()
    {
        return $this->dateArrivee;
    }

    /**
     * Set rueDepart
     *
     * @param string $rueDepart
     *
     * @return Annonce
     */
    public function setRueDepart($rueDepart)
    {
        $this->rueDepart = $rueDepart;

        return $this;
    }

    /**
     * Get rueDepart
     *
     * @return string
     */
    public function getRueDepart()
    {
        return $this->rueDepart;
    }

    /**
     * Set rueArrivee
     *
     * @param string $rueArrivee
     *
     * @return Annonce
     */
    public function setRueArrivee($rueArrivee)
    {
        $this->rueArrivee = $rueArrivee;

        return $this;
    }

    /**
     * Get rueArrivee
     *
     * @return string
     */
    public function getRueArrivee()
    {
        return $this->rueArrivee;
    }

    /**
     * Set informationsComplementaires
     *
     * @param string $informationsComplementaires
     *
     * @return Annonce
     */
    public function setInformationsComplementaires($informationsComplementaires)
    {
        $this->informationsComplementaires = $informationsComplementaires;

        return $this;
    }

    /**
     * Get informationsComplementaires
     *
     * @return string
     */
    public function getInformationsComplementaires()
    {
        return $this->informationsComplementaires;
    }

    /**
     * Set dateDepartFlexible
     *
     * @param string $dateDepartFlexible
     *
     * @return Fret
     */
    public function setDateDepartFlexible($dateDepartFlexible)
    {
        $this->dateDepartFlexible = $dateDepartFlexible;

        return $this;
    }

    /**
     * Get dateDepartFlexible
     *
     * @return string
     */
    public function getDateDepartFlexible()
    {
        return $this->dateDepartFlexible;
    }

    /**
     * Set dateArriveeFlexible
     *
     * @param string $dateArriveeFlexible
     *
     * @return Fret
     */
    public function setDateArriveeFlexible($dateArriveeFlexible)
    {
        $this->dateArriveeFlexible = $dateArriveeFlexible;

        return $this;
    }

    /**
     * Get dateArriveeFlexible
     *
     * @return string
     */
    public function getDateArriveeFlexible()
    {
        return $this->dateArriveeFlexible;
    }

    /**
     * Set departIsFlexible
     *
     * @param boolean $departIsFlexible
     *
     * @return Fret
     */
    public function setDepartIsFlexible($departIsFlexible)
    {
        $this->departIsFlexible = $departIsFlexible;

        return $this;
    }

    /**
     * Get departIsFlexible
     *
     * @return boolean
     */
    public function getDepartIsFlexible()
    {
        return $this->departIsFlexible;
    }

    /**
     * Set arriveIsFlexible
     *
     * @param boolean $arriveIsFlexible
     *
     * @return Fret
     */
    public function setArriveIsFlexible($arriveIsFlexible)
    {
        $this->arriveIsFlexible = $arriveIsFlexible;

        return $this;
    }

    /**
     * Get arriveIsFlexible
     *
     * @return boolean
     */
    public function getArriveIsFlexible()
    {
        return $this->arriveIsFlexible;
    }
}
