<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretAnimaux
 *
 * @ORM\Table(name="fret_animaux")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretAnimauxRepository")
 */
class FretAnimaux extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="genre", type="string", length=255, nullable=true)
     */
    private $genre;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;


    

    /**
     * Set genre
     *
     * @param string $genre
     *
     * @return FretAnimaux
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return string
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set quantite
     *
     * @param string $quantite
     *
     * @return FretAnimaux
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
}

