<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretAlimentation
 *
 * @ORM\Table(name="fret_alimentation")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretAlimentationRepository")
 */
class FretAlimentation extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poidsCertain", type="boolean", nullable=true)
     */
    private $poidsCertain;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var bool
     *
     * @ORM\Column(name="quantiteCertain", type="boolean", nullable=true)
     */
    private $quantiteCertain;
	
	

    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretAlimentation
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return FretAlimentation
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return FretAlimentation
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return FretAlimentation
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set quantiteCertain
     *
     * @param boolean $quantiteCertain
     *
     * @return FretAlimentation
     */
    public function setQuantiteCertain($quantiteCertain)
    {
        $this->quantiteCertain = $quantiteCertain;

        return $this;
    }

    /**
     * Get quantiteCertain
     *
     * @return bool
     */
    public function getQuantiteCertain()
    {
        return $this->quantiteCertain;
    }
}

