<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Annonce
 *
 * @ORM\Table(name="hel_annonce")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\AnnonceRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type_annonce", type="string")
 * @ORM\DiscriminatorMap({"annonce" = "Annonce", "fret" = "Fret", "trajet" = "Trajet", "voiturelocation" = "VoitureLocation"})
 */
class Annonce
{
	
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\UserBundle\Entity\User")
     */
    private $profil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateExperation", type="datetime", nullable=true)
     */
    private $dateExperation;

    /**
     * @var bool
     *
     * @ORM\Column(name="disponible", type="boolean", nullable=true)
     */
    private $disponible;

	/**
     * @var bool
     *
     * @ORM\Column(name="approbation", type="boolean", nullable=true)
     */
    private $approbation;
	
	/**
     *
     * @ORM\OneToOne(targetEntity="Hellofret\BackEndBundle\Entity\Devis", cascade={"remove"})
     */
    private $devisAccepte;

    public function __construct()
	{
		$this->dateCreation = new \Datetime();
		$this->disponible = false;
		$this->approbation = false;
	}
	
	
	
	
	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Fret
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set profil
     *
     * @param string $profil
     *
     * @return Fret
     */
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return string
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Fret
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateExperation
     *
     * @param \DateTime $dateExperation
     *
     * @return Fret
     */
    public function setDateExperation($dateExperation)
    {
        $this->dateExperation = $dateExperation;

        return $this;
    }

    /**
     * Get dateExperation
     *
     * @return \DateTime
     */
    public function getDateExperation()
    {
        return $this->dateExperation;
    }

    /**
     * Set disponible
     *
     * @param boolean $disponible
     *
     * @return Fret
     */
    public function setDisponible($disponible)
    {
        $this->disponible = $disponible;

        return $this;
    }

    /**
     * Get disponible
     *
     * @return bool
     */
    public function getDisponible()
    {
        return $this->disponible;
    }

    /**
     * Set approbation
     *
     * @param boolean $approbation
     *
     * @return Annonce
     */
    public function setApprobation($approbation)
    {
        $this->approbation = $approbation;

        return $this;
    }

    /**
     * Get approbation
     *
     * @return boolean
     */
    public function getApprobation()
    {
        return $this->approbation;
    }

    /**
     * Set devisAccepte
     *
     * @param boolean $devisAccepte
     *
     * @return Annonce
     */
    public function setDevisAccepte($devisAccepte)
    {
        $this->devisAccepte = $devisAccepte;

        return $this;
    }

    /**
     * Get devisAccepte
     *
     * @return boolean
     */
    public function getDevisAccepte()
    {
        return $this->devisAccepte;
    }
}
