<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FretPharmacie
 *
 * @ORM\Table(name="fret_pharmacie")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretPharmacieRepository")
 */
class FretPharmacie extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\Column(name="natureMarchandise", type="string", length=255, nullable=true)
     */
    private $natureMarchandise;

    /**
     * @var int
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="poidsCertain", type="boolean", nullable=true)
     */
    private $poidsCertain;

    /**
     * @var bool
     *
     * @ORM\Column(name="temperateurDirigee", type="boolean", nullable=true)
     */
    private $temperateurDirigee;


   

    /**
     * Set natureMarchandise
     *
     * @param string $natureMarchandise
     *
     * @return FretPharmacie
     */
    public function setNatureMarchandise($natureMarchandise)
    {
        $this->natureMarchandise = $natureMarchandise;

        return $this;
    }

    /**
     * Get natureMarchandise
     *
     * @return string
     */
    public function getNatureMarchandise()
    {
        return $this->natureMarchandise;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return FretPharmacie
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return int
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set poidsCertain
     *
     * @param boolean $poidsCertain
     *
     * @return FretPharmacie
     */
    public function setPoidsCertain($poidsCertain)
    {
        $this->poidsCertain = $poidsCertain;

        return $this;
    }

    /**
     * Get poidsCertain
     *
     * @return bool
     */
    public function getPoidsCertain()
    {
        return $this->poidsCertain;
    }

    /**
     * Set temperateurDirigee
     *
     * @param boolean $temperateurDirigee
     *
     * @return FretPharmacie
     */
    public function setTemperateurDirigee($temperateurDirigee)
    {
        $this->temperateurDirigee = $temperateurDirigee;

        return $this;
    }

    /**
     * Get temperateurDirigee
     *
     * @return bool
     */
    public function getTemperateurDirigee()
    {
        return $this->temperateurDirigee;
    }
}

