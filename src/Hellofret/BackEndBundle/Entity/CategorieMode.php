<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategorieMode
 *
 * @ORM\Table(name="hel_categorie_mode")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\CategorieModeRepository")
 */
class CategorieMode
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\Mode")
     */
    private $mode;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\BackEndBundle\Entity\TypeFret")
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return CategorieMode
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return CategorieMode
     */
    public function setMode(Mode $mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CategorieMode
     */
    public function setType(TypeFret $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}

