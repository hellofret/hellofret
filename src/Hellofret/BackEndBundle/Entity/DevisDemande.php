<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Devis
 *
 * @ORM\Table(name="hel_devis_demandes")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\DevisRepository")
 */
class DevisDemande
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enlevement_time", type="datetime")
     */
    private $enlevementTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="livraison_time", type="datetime")
     */
    private $livraisonTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="datetime")
     */
    private $expirationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="more_infos", type="text", nullable=true)
     */
    private $moreInfos;

    /**
     * @var bool
     *
     * @ORM\Column(name="approbation", type="boolean", nullable=true)
     */
    private $approbation;

    /**
     * @var int
     *
     * @ORM\Column(name="repetition", type="integer", nullable=true)
     */
    private $repetition;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Hellofret\UserBundle\Entity\User")
     */
    private $profil;

    
	
	/**
     * @var string
     *
     * @ORM\Column(name="date_pub", type="datetime", nullable=true)
     */
    private $datePub;
	
	
	public function __construct()
	{
		$this->repetition = 0;
		$this->datePub = new \Datetime();
	}


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Devis
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set enlevementTime
     *
     * @param \DateTime $enlevementTime
     *
     * @return Devis
     */
    public function setEnlevementTime($enlevementTime)
    {
        $this->enlevementTime = $enlevementTime;

        return $this;
    }

    /**
     * Get enlevementTime
     *
     * @return \DateTime
     */
    public function getEnlevementTime()
    {
        return $this->enlevementTime;
    }

    /**
     * Set livraisonTime
     *
     * @param \DateTime $livraisonTime
     *
     * @return Devis
     */
    public function setLivraisonTime($livraisonTime)
    {
        $this->livraisonTime = $livraisonTime;

        return $this;
    }

    /**
     * Get livraisonTime
     *
     * @return \DateTime
     */
    public function getLivraisonTime()
    {
        return $this->livraisonTime;
    }

    /**
     * Set expirationDate
     *
     * @param \DateTime $expirationDate
     *
     * @return Devis
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set moreInfos
     *
     * @param string $moreInfos
     *
     * @return Devis
     */
    public function setMoreInfos($moreInfos)
    {
        $this->moreInfos = $moreInfos;

        return $this;
    }

    /**
     * Get moreInfos
     *
     * @return string
     */
    public function getMoreInfos()
    {
        return $this->moreInfos;
    }

    /**
     * Set approbation
     *
     * @param boolean $approbation
     *
     * @return Devis
     */
    public function setApprobation($approbation)
    {
        $this->approbation = $approbation;

        return $this;
    }

    /**
     * Get approbation
     *
     * @return bool
     */
    public function getApprobation()
    {
        return $this->approbation;
    }

    /**
     * Set repetition
     *
     * @param integer $repetition
     *
     * @return Devis
     */
    public function setRepetition($repetition)
    {
        $this->repetition = $repetition;

        return $this;
    }

    /**
     * Get repetition
     *
     * @return int
     */
    public function getRepetition()
    {
        return $this->repetition;
    }

    /**
     * Set profil
     *
     * @param string $profil
     *
     * @return Devis
     */
    public function setProfil($profil)
    {
        $this->profil = $profil;

        return $this;
    }

    /**
     * Get profil
     *
     * @return string
     */
    public function getProfil()
    {
        return $this->profil;
    }

    
}

