<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * FretConteneur
 *
 * @ORM\Table(name="fret_conteneur")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretConteneurRepository")
 */
class FretConteneur extends Fret
{
    

    /**
     *
     * @ORM\ManyToMany(targetEntity="Hellofret\BackEndBundle\Entity\Conteneur", cascade={"remove", "persist"})
     */
    private $conteneurs;


    /**
     * Set conteneurs
     *
     * @param string $conteneurs
     *
     * @return FretConteneur
     */
    public function setConteneurs($conteneurs)
    {
        $this->conteneurs = $conteneurs;

        return $this;
    }

    /**
     * Get conteneurs
     *
     * @return string
     */
    public function getConteneurs()
    {
        return $this->conteneurs;
    }
}
