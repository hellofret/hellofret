<?php

namespace Hellofret\BackEndBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * FretVehicule
 *
 * @ORM\Table(name="fret_vehicule")
 * @ORM\Entity(repositoryClass="Hellofret\BackEndBundle\Repository\FretVehiculeRepository")
 */
class FretVehicule extends Fret
{
    

    /**
     * @var string
     *
     * @ORM\ManyToMany(targetEntity="Hellofret\BackEndBundle\Entity\Voiture", cascade={"remove", "persist"})
     */
    private $voitures;


   

    /**
     * Set voitures
     *
     * @param string $voitures
     *
     * @return FretVehicule
     */
    public function setVoitures($voitures)
    {
        $this->voitures = $voitures;

        return $this;
    }

    /**
     * Get voitures
     *
     * @return string
     */
    public function getVoitures()
    {
        return $this->voitures;
    }
}
