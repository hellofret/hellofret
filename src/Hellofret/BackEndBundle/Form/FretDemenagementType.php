<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


class FretDemenagementType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeDemenagement', ChoiceType::class, array(
				'choices'  => array('Petit Studio' => "Petit Studio" ,'Grand studio' => 'Grand studio',	'Appartement' => 'Appartement',			'Maison' => 'Maison','Villa' => 'Villa','Local professionnel' => 'Local professionnel',	'Bureau' => 'Bureau','Autres' => 'Autres')))
            ->add('detailsDemenagement')
			->add('rueDepart')
            ->add('rueArrivee')
            ->add('informationsComplementaires')
			->add('mode', EntityType::class, array('class' => 'HellofretBackEndBundle:Mode','choice_label' => 'titre',))
			->add('categorie', 'entity',
                    array (
                            'label' => 'Cat',
                            'class' => 'HellofretBackEndBundle:CategorieMode',
                            'property' => 'titre',
                            'required' => true))				
			->add('villeDepart')
            ->add('dateDepart', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
			))
            ->add('villeArrivee')
			->add('dateArrivee', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
			))
			->add('titre', 'text')
			->add('departIsFlexible','checkbox', array('required' => false))
			->add('dateDepartFlexible', 'text', array('required' => false))
			->add('arriveIsFlexible','checkbox', array('required' => false))
			->add('dateArriveeFlexible', 'text', array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\FretDemenagement'
        ));
    }
}
