<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PaletteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite', 'integer', array('attr' => array('placeholder' => 'Quantite')))
			->add('volume', ChoiceType::class, array('choices'  => array('100cm*80cm"' => "100cm*80cm" ,'120cm*100cm' => '120cm*100cm')))
            ->add('poids', 'integer', array('attr' => array('placeholder' => 'Poids')))
            ->add('poidsCertain')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\Palette'
        ));
    }
}
