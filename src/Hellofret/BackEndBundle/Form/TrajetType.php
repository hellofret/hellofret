<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class TrajetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text')
			->add('type', ChoiceType::class, array('choices'  => array('Fret supplémentaire' => "Fret supplémentaire" ,'Retour à vide' => 'Retour à vide','Trajet régulier' => 'Trajet régulier'), 'expanded' => true, 'data' => 'Fret supplémentaire'))
			->add('villeDepart')
			->add('dateDepart', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
					'required'  => false,
			))
            ->add('villeArrive')
			->add('dateArrive', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
					'required'  => false,
			))
            ->add('escales')
            ->add('typePeriode', ChoiceType::class, array('choices'  => array('Jour' => "Jour" ,'Semaine' => 'Semaine','Mois' => 'Mois')))
            ->add('nombrePeriode')
			->add('dateExpiration', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
			))
            ->add('mode', EntityType::class, array('class' => 'HellofretBackEndBundle:Mode','choice_label' => 'titre',))
			->add('informationsComplementaires')
			->add('departIsFlexible','checkbox', array('required' => false))
			->add('dateDepartFlexible', 'text', array('required' => false))
			->add('arriveIsFlexible','checkbox', array('required' => false))
			->add('dateArriveeFlexible', 'text', array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\Trajet'
        ));
    }
}
