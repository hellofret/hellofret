<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Hellofret\BackEndBundle\Repository\CategorieModeRepository;

class VoitureLocationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('titre')
			->add('categorie', 'entity',
                    array (
                            'label' => 'Cat',
                            'class' => 'HellofretBackEndBundle:CategorieMode',
                            'property' => 'titre',
							'query_builder' => function (CategorieModeRepository $er) {
								return $er->createQueryBuilder('c')
									->where('c.mode = :mode')
									->setParameter('mode', 5)
									->orderBy('c.id', 'ASC');
							},
                            'required' => true))
            ->add('marque')
            ->add('modele')
            ->add('informations', 'textarea', array('required' => false))
            ->add('prix')
            ->add('ville')
			->add('photos', 'collection', array(
				'type'         => new ImageType(),
				'allow_add'    => true,
				'allow_delete' => true ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\VoitureLocation'
        ));
    }
}
