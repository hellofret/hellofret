<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Hellofret\BackEndBundle\Repository\CategorieModeRepository;
use Doctrine\ORM\QueryBuilder;

use Symfony\Component\Form\Extension\Core\Type\DateType;

class FretType extends AbstractType
{
    
	public function __construct()
	{
		$this->modeId = 1;
	}
	
	
	/**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $modeId = $this->modeId;
		$builder
			->add('titre', 'text')
            ->add('mode', EntityType::class, array('class' => 'HellofretBackEndBundle:Mode','choice_label' => 'titre',))
			->add('categorie', 'entity',
                    array (
                            'label' => 'Cat',
                            'class' => 'HellofretBackEndBundle:CategorieMode',
                            'property' => 'titre',
                            'required' => true))				
							
							
			->add('villeDepart')
            ->add('dateDepart', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
			))
            ->add('villeArrivee')
			->add('dateArrivee', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
			))
			->add('departIsFlexible','checkbox', array('required' => false))
			->add('dateDepartFlexible', 'text', array('required' => false))
			->add('arriveIsFlexible','checkbox', array('required' => false))
			->add('dateArriveeFlexible', 'text', array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\Fret'
        ));
    }
}
