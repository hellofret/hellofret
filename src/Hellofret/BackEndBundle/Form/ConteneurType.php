<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ConteneurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite', 'integer', array('attr' => array('placeholder' => 'Quantite')))
			->add('volume', ChoiceType::class, array('choices'  => array('20' => "20’" ,'30' => '30’', '40' => '40’','45' => '45’')))
			->add('type', ChoiceType::class, array('choices'  => array('DRY' => "DRY" ,'FLAT' => 'FLAT', 'HC' => 'HC','OPEN TOP' => 'OPEN TOP','PW' => 'PW', 'REEFER' => 'REEFER')))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\Conteneur'
        ));
    }
}
