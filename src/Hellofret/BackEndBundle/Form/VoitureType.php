<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VoitureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantite', 'integer', array('attr' => array('placeholder' => 'Quantité')))
			->add('modele', 'text', array('attr' => array('placeholder' => 'Modèle')))
            ->add('etat', ChoiceType::class, array('choices'  => array('Roulant"' => "Roulant" ,'En panne' => 'En panne','Accidenté' => 'Accidenté')))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\Voiture'
        ));
    }
}
