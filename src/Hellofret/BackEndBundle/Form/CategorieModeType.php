<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CategorieModeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
			->add('mode', EntityType::class, array('class' => 'HellofretBackEndBundle:Mode','choice_label' => 'titre',))
            ->add('type', EntityType::class, array('class' => 'HellofretBackEndBundle:TypeFret','choice_label' => 'titre',))
            
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\CategorieMode'
        ));
    }
}
