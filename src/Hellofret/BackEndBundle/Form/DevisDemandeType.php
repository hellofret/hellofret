<?php

namespace Hellofret\BackEndBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DevisDemandeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('prix')
			->add('enlevementTime', DateTimeType::class, array(
					'input'  => 'datetime',
					'widget' => 'single_text',
			))
			->add('livraisonTime', DateTimeType::class, array(
					'input'  => 'datetime',
					'widget' => 'single_text',
			))
			->add('expirationDate', DateType::class, array(
					'input'  => 'datetime',
					'format' => 'd/M/y',
					'widget' => 'single_text',
			))
            ->add('moreInfos')
            /*->add('approbation')
            ->add('repetition')
            ->add('profil')
            ->add('annonce')*/
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Hellofret\BackEndBundle\Entity\DevisDemande'
        ));
    }
}
