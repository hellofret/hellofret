<?php

namespace Hellofret\BackEndBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
/**
 * FretRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class FretRepository extends \Doctrine\ORM\EntityRepository
{
	
	public function fretEnCours($profil)
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.dateDepart > :dateNow')
			->setParameter('dateNow', new \Datetime(date('Y/m/d')))
			->andWhere('f.profil = :profil')
			->setParameter('profil', $profil )
			->andWhere('f.approbation = false')
			->orderBy('f.dateCreation', 'DESC')
		  ;
		  return $qb
			->getQuery()
			->getResult();
	}
	
	public function AllFret($profil)
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.profil = :profil')
			->setParameter('profil', $profil)
			->orderBy('f.dateCreation', 'DESC')
		  ;
		  return $qb
			->getQuery()
			->getResult();
	}
	public function FretEnregistree($listfrets)
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.id IN (:listfretenregistre)')
			->setParameter('listfretenregistre', $listfrets)
			->orderBy('f.dateCreation', 'DESC')
		  ;
		  return $qb
			->getQuery()
			->getResult();
	}
	public function FretDisponible()
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.disponible = 1')
			->andWhere('f.dateDepart > :dateNow')
			->setParameter('dateNow', new \Datetime(date('Y/m/d')))
			->andWhere('f.approbation = false')
			->orderBy('f.dateCreation', 'DESC')
		  ;
		  return $qb
			->getQuery()
			->getResult();
	}
	
	public function FretsCible($fretsin)
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.disponible = 1')
			->andWhere('f.dateDepart > :dateNow')
			->setParameter('dateNow', new \Datetime(date('Y/m/d')))
			->andWhere('f.approbation = false')
			->andWhere('f.id In (:ids)')
			->setParameter('ids', $fretsin)
			->orderBy('f.dateCreation', 'DESC')
		  ;
		  return $qb
			->getQuery()
			->getResult();
	}
	
	public function getContentFret($id)
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.id = :id')
			->setParameter('id', $id)
			->leftJoin('f.mode', 'mode')
    		->addSelect('mode')
			->leftJoin('f.categorie', 'categorie')
    		->addSelect('categorie')
		  ;
		  
		  return $qb
			->getQuery()
			->getOneOrNullResult();
	}
	public function FretSearch($mode,$categorie,$depart,$arrivee,$dateDepart,$dateArrivee)
	{
		  $qb = $this->createQueryBuilder('f');
		  $qb
			->where('f.disponible = 1')
			->andWhere('f.dateDepart > :dateNow')
			->setParameter('dateNow', new \Datetime(date('Y/m/d')))
			->andWhere('f.approbation = false')
			->orderBy('f.dateCreation', 'DESC')
		  ;
		  
		  if( $mode ){
			  $qb
				->andWhere('f.mode = :mode')
				->setParameter('mode', $mode) ;
		  }
		  if( $categorie ){
			  $qb
				->andWhere('f.categorie = :categorie')
				->setParameter('categorie', $categorie) ;
		  }
		  if( $depart ){
			  $depart = explode(",", $depart);
			  $qb
				->andWhere('f.villeDepart like :villeDepart')
				->setParameter('villeDepart', '%'.$depart[0].'%') ;
		  }
		  if( $arrivee ){
			  $arrivee = explode(",", $arrivee);
			  $qb
				->andWhere('f.villeArrivee like :villeArrivee')
				->setParameter('villeArrivee', '%'.$arrivee[0].'%') ;
		  }
		  
		  if( $dateDepart ){
			  $dateStartD =  \DateTime::createFromFormat("d/m/Y H:i:s",$dateDepart." 00:00:00");
			  $dateEndD =  \DateTime::createFromFormat("d/m/Y H:i:s",$dateDepart." 23:59:59");
			  $qb
			  	->andWhere('f.dateDepart BETWEEN :dateStartD AND :dateEndD')
				->setParameter('dateStartD', $dateStartD )
				->setParameter('dateEndD', $dateEndD );
		  }
		  
		  if( $dateArrivee ){
			  
			  $dateStartA =  \DateTime::createFromFormat("d/m/Y H:i:s",$dateArrivee." 00:00:00");
			  $dateEndA =  \DateTime::createFromFormat("d/m/Y H:i:s",$dateArrivee." 23:59:59");
			  $qb
			  	->andWhere('f.dateArrivee BETWEEN :dateStartA AND :dateEndA')
				->setParameter('dateStartA', $dateStartA )
				->setParameter('dateEndA', $dateEndA );
			  
		  }
		  
		  
		  return $qb
			->getQuery()
			->getResult();
	}
	
}
