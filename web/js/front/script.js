$(document).ready(function(){
  		$(".owl-carousel").owlCarousel({
  			nav:true,
        // autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
  			loop:true,
        responsive:{
            0:{
                items:1
            },
            800:{
                items:2
            },
            1173:{
                items:3
            }
        }

  		});
  		$(".owl-carousel1").owlCarousel({
  			nav:false,
        autoplay:true,
        autoplayTimeout:2500,
        autoplayHoverPause:true,
  			items : 4,
  			loop:true,
        responsive:{
            0:{
                items:1
            },
            500:{
                items:3
            },
            1173:{
                items:5
            }
        }
  		});

  		$(".aide-btn-head").click(function(){
  			
  			if( $(this).parent().hasClass('active') ){
  				$(".aide-btn-content").slideUp()
  				setTimeout(function  () { 
	  				  $(".aide-btn").removeClass("active");
	  			},500)
  			}else{
  				$(this).parent().addClass("active");
  				setTimeout(function  () {
	  				  $(".aide-btn-content").slideDown()
	  			},500)
  			}

  		})

      $('.toggle-menu').click(function () {
       $(this).next().slideToggle()
       return false
      })

      $('.block-search .typeForm input').change(function () {
        var dataForm = '.'+$(this).attr('data-form');
        $('.block-search form').not(dataForm).slideUp()
        setTimeout(function () {
          $('.block-search '+dataForm).slideDown()
        },500)
      })

     // header fixed  
      var myHeader = $('#header-site');
      myHeader.data( 'position', myHeader.position() );
      $(window).scroll(function(){
          var hPos = myHeader.data('position'), scroll = getScroll();
          if ( hPos.top < scroll.top - 102){
              myHeader.addClass('fixed');
              $('body').addClass('header-fixed');
          }
          else {
              myHeader.removeClass('fixed');
              $('body').removeClass('header-fixed');
          }
      });

      function getScroll () {
          var b = document.body;
          var e = document.documentElement;
          return {
              left: parseFloat( window.pageXOffset || b.scrollLeft || e.scrollLeft ),
              top: parseFloat( window.pageYOffset || b.scrollTop || e.scrollTop )
          };
      }

      /////// login page-wrapper
      var heightHeader = parseInt($('.header').css('height'))
      var heightFooter = parseInt($('.header').css('height'))
      var heightWindow = parseInt($(window).height()) 
      $('.calcul-height').css('min-height',heightWindow - (heightHeader + heightFooter+71))
      /////////////////////////
});