// Javascript


//////////////////////////// traitement des champs insertain ///////////////////
$('.block-incertain input[type=checkbox]').change(function(){
  
  if( this.checked ) {
    $(this).parents('.block-incertain')
        .find('input[type=number],input[type=text]')
        .addClass('disabled').attr('disabled','disabled')
  }else{
    $(this).parents('.block-incertain')
        .find('input[type=number],input[type=text]')
        .removeClass('disabled').removeAttr('disabled','disabled')
  
  } 
})

$('.block-incertain input[type=checkbox]').each(function(){
  
  if( this.checked ) {
    $(this).parents('.block-incertain')
        .find('input[type=number],input[type=text]')
        .addClass('disabled').attr('disabled','disabled')
  }else{
    $(this).parents('.block-incertain')
        .find('input[type=number],input[type=text]')
        .removeClass('disabled').removeAttr('disabled','disabled')
  
  } 
})


////////////////////////////////////////////////////////////////////////////////

////////////////////// Message de confirmation /////////////////////////////////
$('.confirmation').click(function(e) {
	e.preventDefault();
	if( $(this).attr('data-message') ){
		$('.modal .modal-body').text($(this).attr('data-message'));
	}
	$('.modal').fadeIn();
	$('.modal').find('.modal-footer a').attr('href',$(this).attr('href')); 
	$('.modal').find('.modal-footer a').attr('data-form',$(this).attr('data-form')); 
}) 
$('.modal .close-modal, .modal-footer a').click(function(e) {
	if($(this).attr('data-form')){
		e.preventDefault();
		$($(this).attr('data-form'))[0].submit()
		$('.modal').fadeOut();
	}
	$('.modal').fadeOut();
})
////////////////////////////////////////////////////////////////////////////////

////////////////////// les accorianns  /////////////////////////////////
$('.box-header').click(function() {
	$(this).next().slideToggle(); 
	if( $(this).find('.btn-box-tool .fa').hasClass('fa-minus') ){
		$(this).find('.btn-box-tool .fa').removeClass('fa-minus').addClass('fa-plus');
	}else{
		$(this).find('.btn-box-tool .fa').removeClass('fa-plus').addClass('fa-minus')
	}
})  
////////////////////////////////////////////////////////////////////////////////
